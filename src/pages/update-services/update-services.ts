import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-update-services',
  templateUrl: 'update-services.html',
})
export class UpdateServicesPage {
  data;
  services;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.data = this.navParams.get('data');
    console.log(this.data);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateServicesPage');
  }

}
