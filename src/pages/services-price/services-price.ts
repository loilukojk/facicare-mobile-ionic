import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, LoadingController, Platform } from 'ionic-angular';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { startTimeRange } from '@angular/core/src/profile/wtf_impl';
import { ServiceProvider } from "../../services/GlobalVar";
import { Storage } from '@ionic/storage';
import moment from 'moment';
import { File } from '@ionic-native/file';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { FileOpener } from '@ionic-native/file-opener';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { OrderTrackingPage } from '../order-tracking/order-tracking';

@Component({
  selector: 'page-services-price',
  templateUrl: 'services-price.html',
})
export class ServicesPricePage {

  data: any = [];
  id: number;
  note = '';
  txtcounter: number;
  isEdit: boolean = false;
  price: any;
  dataTest: any;
  hasContract = 0;
  hasPriceQuote = 0;
  noteisNULL = false;

  constructor(private iab: InAppBrowser, private fileOpener: FileOpener, public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams,
    public transfer: FileTransfer, public file: File, public document: DocumentViewer, private viewCtrl: ViewController, private payPal: PayPal, public localStorage: Storage,
    public platform: Platform, public alertController: AlertController, public serviceProvider: ServiceProvider) {
    this.data = this.navParams.get('dt');
    this.id = this.navParams.get('id');
    this.deleteSymbol();
    console.log("dtaaaa", this.data);
    console.log(this.id);
    this.serviceProvider.getAPIKey().subscribe(res => {
      let result = <any>res;
      if (result.data_item != undefined) {
        this.serviceProvider.token = result.data_item.apiKey;
        // console.log("APIKey: ", this.serviceProvider.token);
      }
    })

  }

  deleteSymbol() {
    let str = this.data.content.orderValue.sum.split(".").join("");

    this.price = Number((Number(str) / 24000).toFixed(2));
  }

  openContract(link: string) {

    // this.iab.create("https://docs.google.com/viewer?url=" + link, '_system');
    this.iab.create(link, '_system');
    // const browser = this.iab.create("https://19of32x2yl33s8o4xza0gf14-wpengine.netdna-ssl.com/wp-content/uploads/Exhibit-A-SAMPLE-CONTRACT.pdf", '_system');
    // browser.show();
  }

  gotoOrderTracking() {
    this.navCtrl.push(OrderTrackingPage, { orderID: this.data.id });
  }

  openContract2(link: string) {
    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ..."
    });
    loader.present();

    const options: DocumentViewerOptions = {
      title: "Hợp đồng mẫu"
    };
    let path = null;
    let str = link.split("/");


    // link ="https://19of32x2yl33s8o4xza0gf14-wpengine.netdna-ssl.com/wp-content/uploads/Exhibit-A-SAMPLE-CONTRACT.pdf";

    console.log(link);
    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    }
    else {
      path = this.file.dataDirectory;
    }

    const transfer = this.transfer.create();
    transfer.download(link, this.file.documentsDirectory + str[str.length - 1]).then((entry) => {
      let url = entry.toURL();

      this.document.viewDocument(url, 'application/pdf', {});

      loader.dismiss();
    },
      (err) => {
        alert('Lỗi');
        loader.dismiss();
      });
  }

  openContract1(link: string) {
    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ..."
    });
    loader.present();

    let path = null;


    // link ="https://19of32x2yl33s8o4xza0gf14-wpengine.netdna-ssl.com/wp-content/uploads/Exhibit-A-SAMPLE-CONTRACT.pdf";


    let str = link.split("/");

    console.log(link);

    if (this.platform.is('ios')) {
      path = this.file.documentsDirectory;
    }
    else {
      path = this.file.dataDirectory;
    }

    const transfer = this.transfer.create();
    transfer.download(link, this.file.dataDirectory + str[str.length - 1]).then(entry => {
      let url = entry.toURL();
      loader.dismiss();
      this.document.viewDocument(url, 'application/pdf', {});

      // if (this.platform.is('ios')) {
      //   this.document.viewDocument(url, 'application/pdf', {});
      // }
      // else {
      //   this.fileOpener.open(url, 'application/pdf');
      // }
    },
      (err) => {
        alert('Lỗi');
        loader.dismiss();
      });
  }

  // acceptOrders() {
  //   if (this.data.content.paymentInfo.paymentMethod == '1') {
  //     this.makePayment();
  //   }
  //   else {
  //     if (this.hasContract == 0) {
  //       this.serviceProvider.updateOrder(this.data.content.id, "\"3\"").subscribe((data) => {
  //         this.viewCtrl.dismiss();
  //         this.presentAlertNoti();
  //         this.updateStatus(this.data.content.id, "\"3\"");
  //         this.serviceProvider.editNotification(this.data.content.id, "\"3\"");
  //       }, (err) => {
  //         alert("Update trạng thái fail!");
  //       });
  //     }

  //     else {
  //       this.serviceProvider.updateOrder(this.data.content.id, "\"2a\"").subscribe((data) => {
  //         this.serviceProvider.updateOrderContract(this.data.content.id, "\"1\"").subscribe((data) => {
  //           this.viewCtrl.dismiss();
  //           this.presentAlertNoti();
  //           this.updateStatus(this.data.content.id, "\"2a\"");
  //           this.serviceProvider.editNotification(this.data.content.id, "\"2a\"");
  //         }, (err) => {
  //           alert("Update trạng thái fail!");
  //           console.log(err)
  //         });
  //       }, (err) => {
  //         alert("Update trạng thái fail!");
  //       });
  //     }
  //   }
  // }

  acceptOrders() {
    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ..."
    });
    loader.present();

    if (this.hasContract == 1) {
      if (this.data.content.paymentInfo.paymentMethod == '1') {
        // this.makePayment();
        let paymentInfo = {
          "invoice": {},
          "paymentDate": "",
          "paymentMethod": "0",
          "status": "0"
        };

        let body = {
          "content.paymentInfo": paymentInfo,
          "content.status": "2a",
          "content.hasContract": "1"
        }

        this.serviceProvider.updateManyField(this.data.content.id, body).subscribe((data) => {

          let newMessage = {
            from: "mobile",
            to: "crm",
            datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
            content: {
              mobileChannel: this.serviceProvider.idOfDevice,
              currentOrderStatus: "2a",
              orderID: this.data.content.id,
              id_incr: this.data.id_incr,
              msg: `Khách hàng đã chấp nhận báo giá`
            },
            isNew: "1"
          };

          let id = moment().format('DDMMYYYYHHmmss').toString() + this.id;

          this.serviceProvider.publishMessage(newMessage, id);

          loader.dismiss();
          this.viewCtrl.dismiss();
          this.presentAlertNoti();
          this.updateStatus(this.data.content.id, "\"2a\"");
          this.serviceProvider.editNotification(this.data.content.id, "\"2a\"");
        }, (err) => {
          loader.dismiss();
          alert("Update fail!");
          console.log(err)
        });

      }
      else {
        let body = {
          "content.status": "2a",
          "content.hasContract": "1"
        }

        this.serviceProvider.updateManyField(this.data.content.id, body).subscribe((data) => {
          let newMessage = {
            from: "mobile",
            to: "crm",
            datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
            content: {
              mobileChannel: this.serviceProvider.idOfDevice,
              currentOrderStatus: "2a",
              orderID: this.data.content.id,
              id_incr: this.data.id_incr,
              msg: `Khách hàng đã chấp nhận báo giá`
            },
            isNew: "1"
          };

          let id = moment().format('DDMMYYYYHHmmss').toString() + this.id;

          this.serviceProvider.publishMessage(newMessage, id);

          loader.dismiss();
          this.viewCtrl.dismiss();
          this.presentAlertNoti();
          this.updateStatus(this.data.content.id, "\"2a\"");
          this.serviceProvider.editNotification(this.data.content.id, "\"2a\"");
        }, (err) => {
          loader.dismiss();
          alert("Update fail!");
          console.log(err)
        });

      }
    }

    if (this.hasContract == 0) {
      if (this.data.content.paymentInfo.paymentMethod == '1') {
        loader.dismiss();
        this.makePayment();
      }
      else {
        if (this.hasContract == 0) {
          this.serviceProvider.updateOrder(this.data.content.id, "\"3\"").subscribe((data) => {

            let newMessage = {
              from: "mobile",
              to: "crm",
              datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
              content: {
                mobileChannel: this.serviceProvider.idOfDevice,
                currentOrderStatus: "3",
                orderID: this.data.content.id,
                id_incr: this.data.id_incr,
                msg: `Khách hàng đã chấp nhận báo giá`
              },
              isNew: "1"
            };

            let id = moment().format('DDMMYYYYHHmmss').toString() + this.id;

            this.serviceProvider.publishMessage(newMessage, id);

            loader.dismiss();
            this.viewCtrl.dismiss();
            this.presentAlertNoti();
            this.updateStatus(this.data.content.id, "\"3\"");
            this.serviceProvider.editNotification(this.data.content.id, "\"3\"");
          }, (err) => {
            loader.dismiss();
            alert("Update trạng thái fail!");
          });
        }
      }
    }

  }

  txtMaxLengthCheck(e) {
    this.txtcounter = e.length

  }

  editOrders() {
    this.isEdit = !this.isEdit;
  }

  cancelOrders() {
    // this.viewCtrl.dismiss();
    this.presentAlertConfirm();

  }

  submit() {
    if (this.hasPriceQuote == 1) {
      if (this.note == '') {
        this.noteisNULL = true;
      }
      else {
        this.noteisNULL = false;
        let data = this.data.content.priceQuoteRequests;
        data.push(this.note);

        let priceQuoteTimes = +this.data.content.priceQuoteTimes + 1;

        let body = {
          "content.status": "1a",
          "content.priceQuoteRequests": data,
          "content.priceQuoteTimes": priceQuoteTimes.toString()
        }

        this.serviceProvider.updateManyField(this.data.id, body).subscribe(res => {
          alert("Phản hồi của Qúy khách đã được gởi thành công.SAFE HOUSE sẽ liên hệ đến Qúy khách sớm nhất.Cảm ơn Qúy khách!");

          let newMessage = {
            from: "mobile",
            to: "crm",
            datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
            content: {
              mobileChannel: this.serviceProvider.idOfDevice,
              currentOrderStatus: "1a",
              orderID: this.data.content.id,
              id_incr: this.data.id_incr,
              msg: "Khách hàng yêu cầu gửi lại báo giá"
            },
            isNew: "1"
          };

          let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;

          this.serviceProvider.publishMessage(newMessage, id);

        });

        this.viewCtrl.dismiss();
      }
    }
    else {
      if (this.note == '') {
        this.acceptOrders();
      }

      else {
        if (this.hasContract == 0) {
          if (this.data.content.paymentInfo.paymentMethod == '1') {
            this.makePayment();
          }
          else {
            const loader = this.loadingCtrl.create({
              content: "Vui lòng chờ..."
            });
            loader.present();

            let body = {
              "content.status": "2",
              "content.note": this.note
            }

            // this.data.content.status = "2";
            this.serviceProvider.updateManyField(this.data.content.id, body).subscribe((data) => {

              let newMessage = {
                from: "mobile",
                to: "crm",
                datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
                content: {
                  mobileChannel: this.serviceProvider.idOfDevice,
                  currentOrderStatus: "2",
                  orderID: this.data.content.id,
                  id_incr: this.data.id_incr,
                  msg: `Đơn hàng đã phản hồi, cần chỉnh sửa`
                },
                isNew: "1"
              };

              let id = moment().format('DDMMYYYYHHmmss').toString() + this.id;

              this.serviceProvider.publishMessage(newMessage, id);

              loader.dismiss();
              alert("Phản hồi của Qúy khách đã được gởi thành công.SAFE HOUSE sẽ liên hệ đến Qúy khách sớm nhất.Cảm ơn Qúy khách!");
              this.serviceProvider.editNotification(this.data.content.id, "\"2\"");
              this.updateStatus(this.data.content.id, "\"2\"");
              this.navCtrl.pop();
            }, (err) => {
              loader.dismiss();
              alert("Update trạng thái fail!");
              console.log(err)
            });
          }
        }

        if (this.hasContract == 1) {
          const loader = this.loadingCtrl.create({
            content: "Vui lòng chờ..."
          });
          loader.present();

          if (this.data.content.paymentInfo.paymentMethod == '1') {
            // this.makePayment();
            let paymentInfo = {
              "invoice": {},
              "paymentDate": "",
              "paymentMethod": "0",
              "status": "0"
            };

            let body = {
              "content.status": "2",
              "content.note": this.note,
              "content.paymentInfo": paymentInfo,
              "content.hasContract": "1"
            }

            this.serviceProvider.updateManyField(this.data.content.id, body).subscribe((data) => {

              let newMessage = {
                from: "mobile",
                to: "crm",
                datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
                content: {
                  mobileChannel: this.serviceProvider.idOfDevice,
                  currentOrderStatus: "2",
                  orderID: this.data.content.id,
                  id_incr: this.data.id_incr,
                  msg: `Đơn hàng đã phản hồi, cần chỉnh sửa`
                },
                isNew: "1"
              };

              let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;

              this.serviceProvider.publishMessage(newMessage, id);

              loader.dismiss();
              alert("Phản hồi của Qúy khách đã được gởi thành công.SAFE HOUSE sẽ liên hệ đến Qúy khách sớm nhất.Cảm ơn Qúy khách!");
              this.serviceProvider.editNotification(this.data.content.id, "\"2\"");
              this.updateStatus(this.data.content.id, "\"2\"");
              this.navCtrl.pop();
            }, (err) => {
              loader.dismiss();
              alert("Update fail!");
              console.log(err)
            });
          }
          else {

            let body = {
              "content.status": "2",
              "content.note": this.note,
              "content.hasContract": "1"
            }

            // this.data.content.status = "2";
            this.serviceProvider.updateManyField(this.data.content.id, body).subscribe((data) => {

              let newMessage = {
                from: "mobile",
                to: "crm",
                datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
                content: {
                  mobileChannel: this.serviceProvider.idOfDevice,
                  currentOrderStatus: "2",
                  orderID: this.data.content.id,
                  id_incr: this.data.id_incr,
                  msg: `Đơn hàng đã phản hồi, cần chỉnh sửa`
                },
                isNew: "1"
              };

              let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;

              this.serviceProvider.publishMessage(newMessage, id);

              loader.dismiss();
              alert("Phản hồi của Qúy khách đã được gởi thành công.SAFE HOUSE sẽ liên hệ đến Qúy khách sớm nhất.Cảm ơn Qúy khách!");
              this.serviceProvider.editNotification(this.data.content.id, "\"2\"");
              this.updateStatus(this.data.content.id, "\"2\"");
              this.navCtrl.pop();
            }, (err) => {
              loader.dismiss();
              alert("Update trạng thái fail!");
              console.log(err)
            });
          }
        }

      }
    }


  }

  closeOrders() {
    this.viewCtrl.dismiss();
  }

  makePayment() {
    this.payPal.init({
      PayPalEnvironmentProduction: 'YOUR_PRODUCTION_CLIENT_ID',
      PayPalEnvironmentSandbox: 'AX2hkFGXhcGafHJLNmglLfCgsruAe0WpCCot2H-W6ZfEIdhnuFZdW6qzmuye1t79wgW5hg4Yp8Yxfcme'
    }).then(() => {
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        merchantName: 'PCCC',
        acceptCreditCards: true,
        payPalShippingAddressOption: 0,
        rememberUser: true,
        languageOrLocale: 'vi_VN'

      })).then(() => {
        // let payment = new PayPalPayment(this.price, 'VND', this.data.order.orderID, 'thanh toán cho đơn hàng này ', 'sale');
        let payment = new PayPalPayment(`${this.price}`, 'USD', `ID Đơn hàng:${this.data.content.id}`, 'sale');
        console.log(payment)
        this.payPal.renderSinglePaymentUI(payment).then((res) => {
          console.log(payment)

          let paymentInfo = {
            "invoice": res,
            "paymentDate": moment().format("DD/MM/YYYY HH:mm:ss").toString(),
            "paymentMethod": "1",
            "status": "1"
          }

          const loader = this.loadingCtrl.create({
            content: "Vui lòng chờ..."
          });
          loader.present();

          if (this.note != '') {
            this.data.content.status = "2";
            this.data.content.note = this.note;

            let body = {
              "content.status": "2",
              "content.note": this.note,
              "content.paymentInfo": paymentInfo,
            }

            this.serviceProvider.updateManyField(this.data.content.id, body).subscribe((data) => {

              let newMessage = {
                from: "mobile",
                to: "crm",
                datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
                content: {
                  mobileChannel: this.serviceProvider.idOfDevice,
                  currentOrderStatus: "2",
                  orderID: this.data.content.id,
                  id_incr: this.data.id_incr,
                  msg: `Đơn hàng đã thanh toán, cần chỉnh sửa`
                },
                isNew: "1"
              };

              let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;

              this.serviceProvider.publishMessage(newMessage, id);

              loader.dismiss();
              alert('Phản hồi của Qúy khách đã được gởi thành công.SAFE HOUSE sẽ liên hệ đến Qúy khách sớm nhất.Cảm ơn Qúy khách!');
              this.updateStatus(this.data.content.id, "\"2\"");
              this.navCtrl.pop();
              this.serviceProvider.editNotification(this.data.content.id, "\"2\"");

            }, (err) => {
              loader.dismiss();
              alert('Update không thành công!');
            });
          }
          else {
            // if (this.hasContract == 0) {
            this.data.content.status = "6";
            let body = {
              "content.status": "6",
              "content.paymentInfo": paymentInfo,
            }

            this.serviceProvider.updateManyField(this.data.content.id, body).subscribe(
              (res) => {

                let newMessage = {
                  from: "mobile",
                  to: "crm",
                  datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
                  content: {
                    mobileChannel: this.serviceProvider.idOfDevice,
                    currentOrderStatus: "2",
                    orderID: this.data.content.id,
                    id_incr: this.data.id_incr,
                    msg: `Đơn hàng đã thanh toán, cần phân công`
                  },
                  isNew: "1"
                };

                let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;

                this.serviceProvider.publishMessage(newMessage, id);

                console.log({ message: 'updatePaymentInfo Result', res: res });

                loader.dismiss();
                alert('Phản hồi của Qúy khách đã được gởi thành công.SAFE HOUSE sẽ liên hệ đến Qúy khách sớm nhất.Cảm ơn Qúy khách!');
                this.updateStatus(this.data.content.id, "\"6\"");
                this.serviceProvider.editNotification(this.data.content.id, "\"6\"");
                this.navCtrl.pop();
              },
              (err) => {
                console.log(err);
                loader.dismiss();
              });
          }

          // this.serviceProvider.updateOrder(this.data.content.id, this.data.content);
        }, (err) => {
          // Error or render dialog closed without being successful
          alert('Thanh toán fail!')
          console.log(err)
        });
      }, (err) => {
        // Error in configuration
        alert('Thanh toán fail!')
        console.log(err)
      });
    }, (err) => {
      // Error in initialization, maybe PayPal isn't supported or something else
      alert('Thanh toán fail!')
      console.log(err)
    });
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      title: 'Xác nhận',
      message: 'Bạn có chắc muốn hủy đặt lịch này không?',
      buttons: [
        {
          text: 'Hủy bỏ',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Đồng ý',
          handler: () => {
            const loader = this.loadingCtrl.create({
              content: "Vui lòng chờ..."
            });
            loader.present();

            this.serviceProvider.updateOrder(this.data.content.id, "\"7\"").subscribe((data) => {
              let newMessage = {
                from: "mobile",
                to: "crm",
                datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
                content: {
                  mobileChannel: this.serviceProvider.idOfDevice,
                  currentOrderStatus: "2",
                  orderID: this.data.content.id,
                  id_incr: this.data.id_incr,
                  msg: `Đơn hàng đã hủy`
                },
                isNew: "1"
              };

              let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;
              loader.dismiss();
              this.serviceProvider.publishMessage(newMessage, id);
              console.log(data);
              this.viewCtrl.dismiss();
            }, (err) => {
              loader.dismiss();
              console.log(err)
            });
            this.updateStatus(this.data.content.id, "\"7\"");
          }
        }
      ]
    });

    await alert.present();
  }

  async presentAlertNoti() {
    const alert = await this.alertController.create({
      title: 'Xác nhận',
      message: 'Phản hồi của Qúy khách đã được gởi thành công.SAFE HOUSE sẽ liên hệ đến Qúy khách sớm nhất.Cảm ơn Qúy khách!',
      buttons: [
        {
          text: 'ĐỒNG Ý',
          handler: () => {
            // this.navCtrl.popToRoot();
          }
        }
      ]
    });

    await alert.present();
  }

  async updateStatus(id: string, status: string) {
    var data: any;
    await this.localStorage.get("notificationHistory").then(res => {
      data = JSON.parse(res);
    });

    for (var i = 0; i < data.length; i++) {
      if (data[i].order.content.id == id) {
        data[i].order.content.status = status;
      }
    }

    return await this.localStorage.set("notificationHistory", JSON.stringify(data));
  }


  changeContractType(num) {
    if (num == 1) {
      this.hasContract = 1;
    }
    if (num == 0) {
      this.hasContract = 0;
    }

    console.log("has Contarct", this.hasContract);
  }

  changeContractTypePriceQuote(num) {
    if (num == 1) {
      this.hasPriceQuote = 1;
    }
    if (num == 0) {
      this.hasPriceQuote = 0;
    }

    console.log("has Contarct", this.hasContract);
  }

  changeType() {
    console.log("has Contarct", this.hasContract);
  }

  changeTypePriceQuote() {
    console.log("has Contarct", this.hasPriceQuote);
  }
}
