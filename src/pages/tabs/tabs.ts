import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';
import { HistoryBookPage } from '../history-book/history-book'; 
import { NotificationHistoryPage } from '../notification-history/notification-history';
import { ContactPage } from '../contact/contact';

import { Tabs } from 'ionic-angular';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  @ViewChild('myTabs') tabRef: Tabs;

  tabHomeRoot: any = HomePage;
  tabHistoryRoot: any = HistoryBookPage;
  tabNotificationRoot:any = NotificationHistoryPage;
  tabContactRoot:any = ContactPage;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

  onTabsChange() {
    // Get the previous tab if any
    const previousTab = this.tabRef.previousTab(false);

    if(previousTab) {
      try {
        // Get the navCtrl and pop to the root page
        previousTab.getViews()[0].getNav().popToRoot();
      } catch(exception) {
        // Oops...
        console.error(exception);
      }
    }
  }

}
