import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DetailTaskPage } from '../detail-task/detail-task';
import { ServiceProvider } from '../../services/GlobalVar';

@Component({
  selector: 'page-history-task',
  templateUrl: 'history-task.html',
})
export class HistoryTaskPage {
  history: any = [];
  surveyHistory: any = [];
  isNull = false;
  isSurvey = false;

  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams, public serviceProvider: ServiceProvider) {
  }

  ionViewWillEnter() {

    this.serviceProvider.getAPIKey().subscribe(res => {
      let result = <any>res;
      if (result.data_item != undefined) {
        this.serviceProvider.token = result.data_item.apiKey;
        // console.log("APIKey: ", this.serviceProvider.token);
      }
    });

    if (this.serviceProvider.currentUser != '') {

      const loader = this.loadingCtrl.create({
        content: "Vui lòng chờ..."
      });
      loader.present();

      this.serviceProvider.getHistoryTask().subscribe((data) => {
        loader.dismiss();
        let res = <any>data;
        console.log(res);

        if (res.length == undefined) {
          this.isNull = true;
        }
        else {
          this.history = res;
          console.log(res.length);
        }
      }, (res) => {
        alert("Lỗi hệ thống! Vui lòng thử lại sau");
        loader.dismiss();
      });

      this.serviceProvider.getHistorySurveyTask().subscribe(data => {
        console.log(data);
        let res = <any>data;
        if (res.code == undefined) {
          this.surveyHistory = res;
        }
      });
    }
    else {
      this.history = [];
    }

  }

  toDetail(i) {
    var data = this.history[i];
    this.navCtrl.push(DetailTaskPage, { dt: data });
  }

  toDetail1(i) {
    var data = this.surveyHistory[i];
    this.navCtrl.push(DetailTaskPage, { dt: data });
  }

  changeTask(num) {
    this.isSurvey = num;
  }

}
