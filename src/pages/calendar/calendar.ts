import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { CalendarModalOptions, CalendarModal, CalendarResult } from 'ion2-calendar';
import moment from 'moment';

@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {

  optionsMulti: CalendarModalOptions = {
  };

  date = moment().format();

  type: 'string';// 'string' | 'js-date' | 'moment' | 'time' | 'object'

  constructor(public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CalendarPage');
  }

  onChange() {
    console.log(this.date)
  }

  cancel() {
    this.navCtrl.pop();
  }

  save() {
    this.viewCtrl.dismiss(moment(this.date).format());
  }

}
