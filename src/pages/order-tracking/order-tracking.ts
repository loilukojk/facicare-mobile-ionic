import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { ServiceProvider } from '../../services/GlobalVar';
import moment from 'moment';

@Component({
  selector: 'page-order-tracking',
  templateUrl: 'order-tracking.html',
})
export class OrderTrackingPage {
  data = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public serviceProvider: ServiceProvider, public loadingCtrl: LoadingController) {
    console.log(this.navParams.get("orderID"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderTrackingPage', moment.unix(1592042140).format("DD/MM/YYYY HH:mm:ss"));
    this.serviceProvider.getTrackingOrder(this.navParams.get("orderID")).subscribe(data => {
      let res = <any>data;
      let dt = [];
      if (res.code == 0) {

        console.log(res.data);

        for (let i = 0; i < res.data.length; i++) {

          if (i == 0) {
            //Create order: 0
            dt.push({
              case: 1,
              datetime: Number(res.data[i].timestamp)
            })
          }
          else {
            if (res.data[i].payload.content.status != res.data[i - 1].payload.content.status) {

              //Survey:2
              if (res.data[i].payload.content.status == "0a") {
                dt.push({
                  case: 2,
                  datetime: Number(res.data[i].timestamp)
                })
              }

              //Surveied:3
              if (res.data[i].payload.content.status == "0b") {
                dt.push({
                  case: 3,
                  datetime: Number(res.data[i].timestamp)
                })
              }

              //PriceQuote
              if (res.data[i].payload.content.status == "1") {

                //PriceQuote 0: 4
                if (res.data[i].payload.content.priceQuoteTimes == "0") {
                  dt.push({
                    case: 4,
                    datetime: Number(res.data[i].timestamp)
                  })
                }

                //PriceQuote 1: 6
                if (res.data[i].payload.content.priceQuoteTimes == "1") {
                  dt.push({
                    case: 6,
                    datetime: Number(res.data[i].timestamp)
                  })
                }

                //PriceQuote 2: 8
                if (res.data[i].payload.content.priceQuoteTimes == "2") {
                  dt.push({
                    case: 8,
                    datetime: Number(res.data[i].timestamp)
                  })
                }

                //PriceQuote 3: 10
                if (res.data[i].payload.content.priceQuoteTimes == "3") {
                  dt.push({
                    case: 10,
                    datetime: Number(res.data[i].timestamp)
                  })
                }
              }

              //priceQuoteRequests
              if (res.data[i].payload.content.status == "1a") {
                // //priceQuoteRequests 0: 8
                // if (res.data[i].payload.content.priceQuoteTimes == "0") {
                //   dt.push({
                //     case: 8,
                //     datetime: Number(res.data[i].timestamp)
                //   })
                // }

                //priceQuoteRequests 1: 5
                if (res.data[i].payload.content.priceQuoteTimes == "1") {
                  dt.push({
                    case: 5,
                    datetime: Number(res.data[i].timestamp)
                  })
                }

                //priceQuoteRequests 2: 7
                if (res.data[i].payload.content.priceQuoteTimes == "2") {
                  dt.push({
                    case: 7,
                    datetime: Number(res.data[i].timestamp)
                  })
                }

                //priceQuoteRequests 3: 9
                if (res.data[i].payload.content.priceQuoteTimes == "3") {
                  dt.push({
                    case: 9,
                    datetime: Number(res.data[i].timestamp)
                  })
                }
              }

              //Edit: 12
              if (res.data[i].payload.content.status == "2") {
                dt.push({
                  case: 12,
                  datetime: Number(res.data[i].timestamp)
                })
              }

              //Not send Contract: 13
              if (res.data[i].payload.content.status == "2a") {
                dt.push({
                  case: 13,
                  datetime: Number(res.data[i].timestamp)
                })
              }

              //Sent Contract:14
              if (res.data[i].payload.content.status == "2b") {
                dt.push({
                  case: 14,
                  datetime: Number(res.data[i].timestamp)
                })
              }

              //signed the contract :15
              if (res.data[i].payload.content.status == "2c") {
                dt.push({
                  case: 15,
                  datetime: Number(res.data[i].timestamp)
                })
              }

              //Not done task, not pay: 16
              if (res.data[i].payload.content.status == "3") {
                dt.push({
                  case: 16,
                  datetime: Number(res.data[i].timestamp)
                })
              }

              //Done task, not pay: 17
              if (res.data[i].payload.content.status == "4") {
                dt.push({
                  case: 17,
                  datetime: Number(res.data[i].timestamp)
                })
              }

              //Done task, done pay: 18
              if (res.data[i].payload.content.status == "5") {
                dt.push({
                  case: 18,
                  datetime: Number(res.data[i].timestamp)
                })
              }

              //Not done task, done pay: 19
              if (res.data[i].payload.content.status == "6") {
                dt.push({
                  case: 19,
                  datetime: Number(res.data[i].timestamp)
                })
              }

              //Cancel: 20
              if (res.data[i].payload.content.status == "7") {
                dt.push({
                  case: 20,
                  datetime: Number(res.data[i].timestamp)
                })
              }

              //Pending: 21
              if (res.data[i].payload.content.status == "8") {
                dt.push({
                  case: 21,
                  datetime: Number(res.data[i].timestamp)
                })
              }

              //Admin end survey task: 22
              if (res.data[i].payload.content.status == "0c") {
                dt.push({
                  case: 22,
                  datetime: Number(res.data[i].timestamp)
                })
              }
            }
          }
        }

        this.data = dt.sort((a, b) => {
          let t1 = a.datetime;
          let t2 = b.datetime;

          if (t1 > t2) {
            return 1;
          }
          if (t1 < t2) {
            return -1;
          }

          return 0;
          // return Number(a.data_item.content.index) < Number(b.data_item.content.index)
        });

        console.log("trackinh: ", this.data);
      }
    })
  }

  getDate(datetime){
    return moment.unix(datetime).format("DD/MM/YYYY");
  }

  getTime(datetime){
    return moment.unix(datetime).format("HH:mm:ss");
  }

}
