import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController, ActionSheetController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../services/GlobalVar';
import { ServicesPricePage } from '../services-price/services-price';
import { Badge } from '@ionic-native/badge';
import moment from 'moment';
import { SettingTaskPage } from '../setting-task/setting-task';

@Component({
  selector: 'page-notification-history',
  templateUrl: 'notification-history.html',
})
export class NotificationHistoryPage {

  data;
  isNull = false;

  constructor(public localStorage: Storage, public actionSheetController: ActionSheetController, public popoverCtrl: PopoverController, private badge: Badge, public navCtrl: NavController, public serviceProvider: ServiceProvider, public navParams: NavParams) {
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter NotificationHistoryPage');
    let data = JSON.parse(localStorage.getItem("Notification_History")) || [];

    if (data != null) {
      this.data = data.sort((a, b) => {
        let t1 = moment(a.datetime, 'DD/MM/YYYY HH:mm:ss');
        let t2 = moment(b.datetime, 'DD/MM/YYYY HH:mm:ss');

        if (t1 < t2) {
          return 1;
        }
        if (t1 > t2) {
          return -1;
        }

        return 0;
      });
    }

    this.serviceProvider.getAPIKey().subscribe(res => {
      let result = <any>res;
      if (result.data_item != undefined) {
        this.serviceProvider.token = result.data_item.apiKey;
        // console.log("APIKey: ", this.serviceProvider.token);
      }
    })


    // console.log(JSON.parse(localStorage.getItem("Notification_History")));
    console.log(this.data);

    if (this.data.length == 0) {
      this.isNull = true;
      console.log(this.data);
    }
    else {
      this.isNull = false;
    }
  }

  goDetail(id, data) {
    let noti = JSON.parse(localStorage.getItem("Notification_History"));
    for (let a of noti) {
      if (a.message == data.message && a.datetime == data.datetime && a.orderID == data.orderID) {
        a.isNew = '0';
        localStorage.setItem("Notification_History", JSON.stringify(noti));
        break;
      }
    }

    let numberNoti: number = 0;

    let noti1 = JSON.parse(localStorage.getItem("Notification_History")) || [];
    for (let a of noti1) {
      if (a.isNew == '1') {
        numberNoti += 1;
      }
    }

    this.badge.set(numberNoti);

    this.serviceProvider.getOrder(id).subscribe(res => {
      console.log(res);
      let data = <any>res;
      // let data = JSON.parse(dt);
      this.navCtrl.push(ServicesPricePage, { dt: data.data_item, id: 1 });
    })
  }

  async doRefresh(refresher) {
    this.data = undefined;
    let data = JSON.parse(localStorage.getItem("Notification_History")) || [];

    if (data != null) {
      this.data = data.sort((a, b) => {
        let t1 = moment(a.datetime, 'DD/MM/YYYY HH:mm:ss');
        let t2 = moment(b.datetime, 'DD/MM/YYYY HH:mm:ss');

        if (t1 < t2) {
          return 1;
        }
        if (t1 > t2) {
          return -1;
        }

        return 0;
      });
    }
    console.log(this.data);

    if (this.data.length == 0) {
      this.isNull = true;
      console.log(this.data);
    }
    else {
      this.isNull = false;
    }

    refresher.complete();
  }

  async gotoSetting(e, dt) {
    const actionSheet = await this.actionSheetController.create({
      // header: 'Albums',
      buttons: [{
        text: 'Gỡ thông báo này',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
          this.deleteNoti(dt);
        }
      },
      {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
      ]
    });
    await actionSheet.present();
  }

  deleteNoti(data1) {
    let dt = [];
    let noti = JSON.parse(localStorage.getItem("Notification_History"));

    for (let a of noti) {
      if (a.message == data1.message && a.datetime == data1.datetime && a.orderID == data1.orderID) {
      }
      else {
        dt.push(a);
        // localStorage.setItem("Notification_History", JSON.stringify(dt));
      }
    }

    localStorage.setItem("Notification_History", JSON.stringify(dt));

    let data = JSON.parse(localStorage.getItem("Notification_History")) || [];

    if (dt.length != 0) {
      this.data = dt.sort((a, b) => {
        let t1 = moment(a.datetime, 'DD/MM/YYYY HH:mm:ss');
        let t2 = moment(b.datetime, 'DD/MM/YYYY HH:mm:ss');

        if (t1 < t2) {
          return 1;
        }
        if (t1 > t2) {
          return -1;
        }

        return 0;
      });
    }

    if (this.data.length == 0) {
      this.isNull = true;
      console.log(this.data);
    }
    else {
      this.isNull = false;
    }
  }

  deleteAll() {
    localStorage.setItem("Notification_History", JSON.stringify([]));
    this.data = [];
    this.isNull = true;
  }
}
