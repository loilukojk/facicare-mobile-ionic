import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { DetailHistoryPage } from '../detail-history/detail-history';
import { ServiceProvider } from '../../services/GlobalVar';
import { Storage } from '@ionic/storage';
import { ServicesPricePage } from '../services-price/services-price';
import moment from 'moment';

@Component({
  selector: 'page-history-book',
  templateUrl: 'history-book.html',
})
export class HistoryBookPage {

  // orders = orders.data;
  orders = [];
  isNull = false;
  constructor(public loadingCtrl: LoadingController, public storage: Storage, public navCtrl: NavController,
    public navParams: NavParams, public serviceProvider: ServiceProvider) {
  }

  ionViewWillEnter() {
    this.orders = [];
    let orders = [];

    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ..."
    });
    loader.present();

    this.serviceProvider.getAPIKey().subscribe(res => {
      let result = <any>res;
      if (result.data_item != undefined) {
        this.serviceProvider.token = result.data_item.apiKey;
        // console.log("APIKey: ", this.serviceProvider.token);
      }
    })

    this.storage.get("BookData").then(async res => {
      console.log(res);
      if (res != null) {

        for (let order of JSON.parse(res)) {

          await this.serviceProvider.getOrder(order)
            .subscribe(res => {

              let result = <any>res;

              if (result.data_item != undefined) {
                orders.push(res);
                this.orders = orders.sort((a, b) => {
                  let t1 = a.data_item.created_at;
                  let t2 = b.data_item.created_at;

                  if (t1 < t2) {
                    return 1;
                  }
                  if (t1 > t2) {
                    return -1;
                  }

                  return 0;
                });
              }
            });
        }
      }
      else {
        this.isNull = true;
      }
    });

    loader.dismiss();
  }

  async doRefresh(refresher) {
    this.orders = [];
    let orders = [];

    this.storage.get("BookData").then(async res => {

      if (res != null) {

        for (let order of JSON.parse(res)) {

          await this.serviceProvider.getOrder(order)
            .subscribe(res => {

              let result = <any>res;

              if (result.data_item != undefined) {
                orders.push(res);

                this.orders = orders.sort((a, b) => {
                  let t1 = a.data_item.created_at;
                  let t2 = b.data_item.created_at;

                  if (t1 < t2) {
                    return 1;
                  }
                  if (t1 > t2) {
                    return -1;
                  }
                  return 0;
                });
              }
            })
        }

      }
      else {
        this.isNull = true;
      }
    });

    refresher.complete();
  }


  toDetail(v: number) {
    var data = this.orders[v].data_item;
    console.log(this.orders[v])
    // this.navCtrl.push(DetailHistoryPage, { dt: data });
    this.navCtrl.push(ServicesPricePage, { dt: data, id: v });
  }
}
