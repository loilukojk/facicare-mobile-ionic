import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import moment from 'moment';
import { DetailTaskPage } from '../detail-task/detail-task';
import { ServiceProvider } from '../../services/GlobalVar';
@Component({
  selector: 'page-back-log',
  templateUrl: 'back-log.html',
})
export class BackLogPage {
  tasks: any = [];
  SurveyTasks: any = [];
  currDate = moment().format("MM/DD/YYYY");
  todaytasks: any;
  checkNull = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modal: ModalController,
    public loadingCtrl: LoadingController, public serviceProvider: ServiceProvider) {

  }

  ionViewWillEnter() {

    this.serviceProvider.getAPIKey().subscribe(res => {
      let result = <any>res;
      if (result.data_item != undefined) {
        this.serviceProvider.token = result.data_item.apiKey;
        // console.log("APIKey: ", this.serviceProvider.token);
      }
    })

    this.tasks = [];
    this.SurveyTasks = [];
    this.getData();
  }

  getData() {
    if (this.serviceProvider.currentUser != '') {

      const loader = this.loadingCtrl.create({
        content: "Vui lòng chờ..."
      });
      loader.present();

      this.serviceProvider.getBackLogTask().subscribe((data) => {
        loader.dismiss();
        console.log("Sửa chữa Quá hạn: ", data);
        let res = <any>data;
        if (res.length == undefined) {
          this.checkNull = true;
        }
        else {
          this.tasks = res;
          this.checkNull = false;
        }

        this.serviceProvider.getBackLogSurveyTask().subscribe((data) => {
          console.log("Khảo sát quá hạn", data);
          let res = <any>data;
          if (res.length > 0) {
            this.SurveyTasks = res;
          }
        })

      }, (res) => {
        alert("Lỗi hệ thống! Vui lòng thử lại sau");
        loader.dismiss();
      })
    }
    else {
      this.checkNull = true;
      this.tasks = [];
    }
  }

  toServicesPrice(v: number) {
    var data = this.tasks[v];
    console.log(data);
    this.navCtrl.push(DetailTaskPage, { dt: data });

    console.log("dfgd", data)
  }

  toServicesPrice_Survey(v: number) {
    var data = this.SurveyTasks[v];
    console.log(data);
    this.navCtrl.push(DetailTaskPage, { dt: data, isSurveyTask: true });

    console.log("dfgd", data)
  }

}

