import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import moment from 'moment';
import { DetailTaskPage } from '../detail-task/detail-task';
import { ServiceProvider } from '../../services/GlobalVar';
import { CalendarModalOptions, CalendarModal, CalendarResult } from 'ion2-calendar';


@Component({
  selector: 'page-month-task1',
  templateUrl: 'month-task1.html',
})
export class MonthTask1Page {

  date: string = new Date().toString();
  viewTitle = moment().format("DD/MM/YYYY")
  type: 'string';// 'string' | 'js-date' | 'moment' | 'time' | 'object'
  task = [];
  mon
  daysConfig: any = [];
  dataOfCalendar = [];
  optionsMulti: CalendarModalOptions = {
    canBackwardsSelected: true,
    daysConfig: this.daysConfig
    // color:'primary',
  };
  monthTask: any = [];
  nextMonthTask: any = [];
  pastDay = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modal: ModalController, public serviceProvider: ServiceProvider) {
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter() {
    this.serviceProvider.getAPIKey().subscribe(res => {
      let result = <any>res;
      if (result.data_item != undefined) {
        this.serviceProvider.token = result.data_item.apiKey;
        // console.log("APIKey: ", this.serviceProvider.token);
      }
    })

    this.date = new Date().toString();
    console.log("curUser", this.serviceProvider.currentUser)
    if (this.serviceProvider.currentUser != '') {
      this.getThisMonth();
    }
    else {
      console.log("đã đăng xuất");
      this.daysConfig = [];
      this.task = [];
      this.monthTask = [];
      this.dataOfCalendar = [];
      this.optionsMulti = {};
    }

  }


  getThisMonth() {
    this.task = [];
    this.monthTask = [];
    this.dataOfCalendar = [];
    this.daysConfig = [];

    this.serviceProvider.getMonthTask().subscribe(async (data) => {
      let res = <any>data;
      if (res.length > 0) {
        this.monthTask = await res;

        for (let order of res) {
          let checkExistence = false;
          console.log(order.content.assignInfo.workDate)
          for (let dateData of this.dataOfCalendar) {
            if (moment(order.content.assignInfo.workDate, "DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY") == moment(dateData.date, "DD/MM/YYYY").format("DD/MM/YYYY")) {
              dateData.data.push(order);
              checkExistence = true;
              break;
            }
          }
          if (checkExistence == false) {
            this.dataOfCalendar.push({
              date: moment(order.content.assignInfo.workDate, "DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY"),
              data: [order]
            })
          }
        }
      }
      this.serviceProvider.getMonthSurveyTask().subscribe(async response => {
        let res = <any>response;
        if (res.length > 0) {
          await Array.prototype.push.apply(this.monthTask, res);
          console.log(this.monthTask);

          for (let order of res) {
            let checkExistence = false;
            console.log(order.content.surveyByTechnicianInfo.workDate)
            for (let dateData of this.dataOfCalendar) {
              if (moment(order.content.surveyByTechnicianInfo.workDate, "DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY") == moment(dateData.date, "DD/MM/YYYY").format("DD/MM/YYYY")) {
                dateData.data.push(order);
                checkExistence = true;
                break;
              }
            }
            if (checkExistence == false) {
              this.dataOfCalendar.push({
                date: moment(order.content.surveyByTechnicianInfo.workDate, "DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY"),
                data: [order]
              })
            }
          }
        }

        console.log('Du lieu cho lich thang')
        console.log(this.dataOfCalendar);
        for (let dt of this.dataOfCalendar) {
          console.log(dt.date, ": ", dt.data.length);
          console.log(dt);
          this.daysConfig.push({
            cssClass: 'marked',
            date: new Date(moment(dt.date, "DD/MM/YYYY").format()),
            subTitle: dt.data.length,
            marked: true
          })
        }
        this.onChange();
        this.getNextMonth();
      });

    });
  }
  getNextMonth() {
    this.serviceProvider.getNextMonthTask().subscribe(async (data) => {
      let res = <any>data;
      if (res.length > 0) {
        this.nextMonthTask = await res;

        for (let order of res) {
          let checkExistence = false
          for (let dateData of this.dataOfCalendar) {
            if (moment(order.content.assignInfo.workDate, "DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY") == moment(dateData.date, "DD/MM/YYYY").format("DD/MM/YYYY")) {
              dateData.data.push(order);
              checkExistence = true;
              break;
            }
          }
          if (checkExistence == false) {
            this.dataOfCalendar.push({
              date: moment(order.content.assignInfo.workDate, "DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY"),
              data: [order]
            })
          }
        }
      }

      this.serviceProvider.getNextMonthSurveyTask().subscribe(async response => {
        let res = <any>response;
        if (res.length > 0) {
          await Array.prototype.push.apply(this.monthTask, res);
          console.log(this.monthTask);

          for (let order of res) {
            let checkExistence = false
            for (let dateData of this.dataOfCalendar) {
              if (moment(order.content.surveyByTechnicianInfo.workDate, "DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY") == moment(dateData.date, "DD/MM/YYYY").format("DD/MM/YYYY")) {
                dateData.data.push(order);
                checkExistence = true;
                break;
              }
            }
            if (checkExistence == false) {
              this.dataOfCalendar.push({
                date: moment(order.content.surveyByTechnicianInfo.workDate, "DD/MM/YYYY HH:mm:ss").format("DD/MM/YYYY"),
                data: [order]
              })
            }
          }
        }
        console.log(this.nextMonthTask);


        for (let dt of this.dataOfCalendar) {
          this.daysConfig.push({
            cssClass: 'marked',
            date: new Date(moment(dt.date, "DD/MM/YYYY").format()),
            subTitle: dt.data.length,
            marked: true
          })
        }
        this.optionsMulti = {
          daysConfig: this.daysConfig,
          canBackwardsSelected: true
          // color:'primary',
        };

      })


    })
  }

  today() {
    this.task = [];
    // this.monthTask=[];
    // this.dataOfCalendar=[];
    this.date = new Date().toString();
    this.viewTitle = moment().format("DD/MM/YYYY");

    for (let dt of this.dataOfCalendar) {
      if (moment().format("DD/MM/YYYY") == moment(dt.date, "DD/MM/YYYY").format("DD/MM/YYYY")) {
        for (let task of dt.data) {
          this.task.push(task);
        }
      }
    }
  }

  onChange() {
    console.log("sdfdaf");
    this.task = [];
    this.viewTitle = moment(this.date.toString()).format("DD/MM/YYYY");

    if (moment(this.date).format("DD/MM/YYYY") >= moment().format("DD/MM/YYYY")) {
      this.pastDay = false;

      for (let dt of this.dataOfCalendar) {
        if (moment(this.date).format("DD/MM/YYYY") == moment(dt.date, "DD/MM/YYYY").format("DD/MM/YYYY")) {
          for (let task of dt.data) {
            this.task.push(task);
          }
        }
      }
    }
    else {
      this.pastDay = true;

      for (let dt of this.dataOfCalendar) {
        if (moment(this.date, "DD/MM/YYYY").format("DD/MM/YYYY") == moment(dt.date, "DD/MM/YYYY").format("DD/MM/YYYY")) {
          for (let task of dt.data) {
            if (task.content.status != '5')
              this.task.push(task);
          }

          for (let task of dt.data) {
            if (task.content.status == '5')
              this.task.push(task);
          }
        }
      }

    }

  }

  toDetail(i) {
    var data = this.task[i];
    if (data.content.status == "0a" || data.content.status == "0b") {
      this.navCtrl.push(DetailTaskPage, { dt: data, isSurveyTask: true });
    }
    else
      this.navCtrl.push(DetailTaskPage, { dt: data });
    console.log("dfgd", data)
  }

  checkStatusTask(status, date, dateSurvey) {
    if (status == '7' || status == '1a' || status == '2') {
      return 3;
    }
    if (date == '' || date == undefined) {
      if (status == '0a' && moment(dateSurvey, 'DD/MM/YYYY HH:mm:ss') >= moment()) {
        return 1;
      }
      if (status == '0a' && moment(dateSurvey, 'DD/MM/YYYY HH:mm:ss') < moment()) {
        return 0;
      }
      if (status == '0b') {
        return 2;
      }
    }
    else {
      if ((status == '3' || status == '4' || status == '6') && moment(date, 'DD/MM/YYYY HH:mm:ss') < moment()) {
        return 0;
      }

      if ((status == '3' || status == '4' || status == '6') && moment(date, 'DD/MM/YYYY HH:mm:ss') >= moment()) {
        return 1;
      }

      if (status == '5') {
        return 2;
      }
    }
  }

  checkStatusTask1(status) {
    if (status == '5') {
      return 1;
    }
    else {
      return 0;
    }
  }

}
