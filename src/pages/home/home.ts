import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { BookPage } from '../book/book';
import { ServiceProvider } from '../../services/GlobalVar';
import { ServicesPricePage } from '../services-price/services-price';
import { AccountPage } from '../account/account';

import moment from 'moment';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  data: any = {};
  services: any;
  display = false;
  title = "";

  constructor(public loadingCtrl: LoadingController, public navCtrl: NavController,
    public navParams: NavParams, public serviceProvider: ServiceProvider) {
  }

  setTitle() {
    let currdate = moment().format("DD/MM/YYYY");

    if (moment() > moment(`${currdate} 00:00:00`, "DD/MM/YYYY HH:mm:ss") && moment() <= moment(`${currdate} 12:00:59`, "DD/MM/YYYY HH:mm:ss")) {
      this.title = "Chào buổi sáng";
    }
    else if (moment() > moment(`${currdate} 12:00:59`, "DD/MM/YYYY HH:mm:ss") && moment() <= moment(`${currdate} 14:00:59`, "DD/MM/YYYY HH:mm:ss")) {
      this.title = "Chào buổi trưa";
    }
    else if (moment() > moment(`${currdate} 14:00:59`, "DD/MM/YYYY HH:mm:ss") && moment() <= moment(`${currdate} 18:00:59`, "DD/MM/YYYY HH:mm:ss")) {
      this.title = "Chào buổi chiều";
    }
    else {
      this.title = "Chào buổi tối";
    }
  }

  doRefresh(refresher) {
    // const loader = this.loadingCtrl.create({
    //   content: "Vui lòng chờ..."
    // });
    // loader.present();

    this.services = [];

    this.serviceProvider.getServices().subscribe(dt => {
      let res = <any>dt;
      // this.services = res.data;
      this.services = new Array(12);
      // this.services = res.data;
      this.services = res.data.sort((a, b) => {
        let t1 = a.data_item.content.index;
        let t2 = b.data_item.content.index;

        if (t1 > t2) {
          return 1;
        }
        if (t1 < t2) {
          return -1;
        }

        return 0;
        // return Number(a.data_item.content.index) < Number(b.data_item.content.index)
      });
      // loader.dismiss();
      refresher.complete();
    }, (res) => {
      alert("Lỗi hệ thống! Vui lòng thử lại sau");
      // loader.dismiss();
      refresher.complete();
    });
  }

  ionViewDidEnter() {
    this.data.image = "https://i.imgur.com/U4Oadyu.png";
    this.data.image2 = "https://i.imgur.com/r7uK5St.jpg";

    this.serviceProvider.getAPIKey().subscribe(res => {
      let result = <any>res;
      if (result.data_item != undefined) {
        this.serviceProvider.token = result.data_item.apiKey;
        // console.log("APIKey: ", this.serviceProvider.token);
      }
    })

    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ..."
    });
    loader.present();

    this.setTitle();

    setInterval(data => {
      this.setTitle();
    }, 60000);

    this.serviceProvider.getServices().subscribe(dt => {

      let res = <any>dt;
      this.services = new Array(12);

      this.services = res.data.sort((a, b) => {
        let t1 = a.data_item.content.index;
        let t2 = b.data_item.content.index;

        if (t1 > t2) {
          return 1;
        }
        if (t1 < t2) {
          return -1;
        }

        return 0;
        // return Number(a.data_item.content.index) < Number(b.data_item.content.index)
      });

      console.log(this.services);
      
      this.display = true;
      loader.dismiss();
    }, (res) => {
      alert("Lỗi hệ thống! Vui lòng thử lại sau");
      loader.dismiss();
    });
  }

  goBookPage(service: any) {
    this.serviceProvider.selectedServiceList = [];
    this.serviceProvider.selectedServiceList.push(service);
    this.navCtrl.push(BookPage);
  }

  gotoAccount() {
    this.navCtrl.push(AccountPage);
  }
}
