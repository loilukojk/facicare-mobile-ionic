import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController, PopoverController, ActionSheetController, AlertController } from 'ionic-angular';
import { PayPage } from '../pay/pay';
import { ServiceProvider } from '../../services/GlobalVar';
import moment from 'moment';
import { UpdateServicesPage } from '../update-services/update-services';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MediaCapture, MediaFile, CaptureError, CaptureImageOptions, CaptureVideoOptions } from '@ionic-native/media-capture';
import { ImagePickerOptions, ImagePicker } from '@ionic-native/image-picker';
import { RequestOptions, Http, Headers } from '@angular/http';
import { File, FileEntry } from '@ionic-native/file';

@Component({
  selector: 'page-detail-task',
  templateUrl: 'detail-task.html',
})
export class DetailTaskPage {
  data: any;
  check = false;
  isPayment = false;
  isFinishTask = false;
  status: string;
  chosenServices;
  imageList;
  orderValue;
  videoList;
  isExamine = false;
  services = [];
  imageResponse = [];
  video = '';
  videoOri = '';
  masterFormdataToSend = new FormData();
  id;
  assigneeInfo;
  note = "";
  txtcounter: number;

  constructor(public alertController: AlertController, public imagePicker: ImagePicker, private mediaCapture: MediaCapture, public camera: Camera, public actionSheetController: ActionSheetController,
    public popoverCtrl: PopoverController, public loadingCtrl: LoadingController, public toastController: ToastController, public navCtrl: NavController,
    public navParams: NavParams, public serviceProvider: ServiceProvider, public file: File, private http: Http) {
    this.data = this.navParams.get('dt');
    this.status = this.data.content.status;
    this.id = this.data.id;

    console.log(this.navParams.get('dt'));

    this.serviceProvider.getAPIKey().subscribe(res => {
      let result = <any>res;
      if (result.data_item != undefined) {
        this.serviceProvider.token = result.data_item.apiKey;
        // console.log("APIKey: ", this.serviceProvider.token);
      }
    })

    this.serviceProvider.getServices().subscribe(dt => {
      let res = <any>dt;
      this.services = res.data;
    });

    if (this.data.content.assignInfo.assignee.length > 0) {
      for (let employee of this.data.content.assignInfo.assignee) {
        if (employee.id == this.serviceProvider.currentUser) {
          this.assigneeInfo = employee;
          if (this.assigneeInfo.statusTask == undefined) {
            this.assigneeInfo.statusTask = '0';
          }
          console.log("ssss", this.assigneeInfo);
          break;
        }
      }
    }

    if (this.navParams.get('isSurveyTask') == true) {
      this.isExamine = true;
    }

    if (this.data.content.surveyByTechnicianInfo.content.orderValue == undefined) {
      this.orderValue = this.data.content.orderValue;
      this.chosenServices = this.data.content.chosenServices;
      this.imageList = this.data.content.imageList;
      this.video = this.data.content.videoList;
      this.videoOri = this.data.content.videoList;
      console.log(this.orderValue);
    }
    else {
      this.orderValue = this.data.content.surveyByTechnicianInfo.content.orderValue;
      this.chosenServices = this.data.content.surveyByTechnicianInfo.content.chosenServices;
      this.imageList = this.data.content.imageList;
      this.video = this.data.content.videoList;
      this.videoOri = this.data.content.videoList;
    }

    // if (moment(this.data.content.assignInfo.deadline, 'DD/MM/YYYY HH:mm:ss') >= moment()
    //   && moment() >= moment(this.data.content.assignInfo.workDate, 'DD/MM/YYYY HH:mm:ss')) {
    //   this.check = true;
    // }

    if (moment(this.data.content.assignInfo.deadline, 'DD/MM/YYYY HH:mm:ss') >= moment()) {
      this.check = true;
    }


    if (this.data.content.status == "4" || this.data.content.status == "5" || this.data.content.status == "7" || this.data.content.status == "8") {
      this.isFinishTask = true;
    }
    if (this.data.content.status == "5" || this.data.content.status == "6" || this.data.content.status == "7" || this.data.content.status == "8") {
      this.isPayment = true;
    }
  }

  finishTask() {
    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ..."
    });
    loader.present();

    for (let employee of this.data.content.assignInfo.assignee) {
      if (employee.id == this.serviceProvider.currentUser) {
        employee.statusTask = '1';
        this.serviceProvider.updateAssignee(this.data.id, this.data.content.assignInfo.assignee).subscribe(data => {

          loader.dismiss();

          let toast = this.toastController.create({
            message: 'Bạn đã hoàn thành công việc',
            duration: 3000
          });

          toast.present();

          this.assigneeInfo.statusTask = '1';
        })
        break;
      }
    }
  }


  confirmFinish() {
    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ..."
    });
    loader.present();

    if (this.status == "3") {
      for (let a of this.data.content.assignInfo.assignee) {
        if (a.isTeamLeader != true)
          a.statusTask = '1';
      };

      this.status = "4";
      let status = "\"4\"";

      let body = {
        "content.status": this.status,
        "content.assignInfo.assignee": this.data.content.assignInfo.assignee

      }

      this.serviceProvider.updateManyField(this.data.id, body).subscribe(data => {

        let newMessage = {
          from: "mobile",
          to: "crm",
          datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
          content: {
            mobileChannel: this.serviceProvider.idOfDevice,
            currentOrderStatus: "4",
            orderID: this.data.id,
            id_incr: this.data.id_incr,
            msg: `Đơn hàng đã hoàn tất công việc, chưa thanh toán`
          },
          isNew: "1"
        };

        let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;

        this.serviceProvider.publishMessage(newMessage, id);
        this.serviceProvider.publishMessagetoCustomer(newMessage, id, this.data.content.mobileChannel);

        loader.dismiss();
        this.isFinishTask = true;
        let toast = this.toastController.create({
          message: 'Bạn đã hoàn thành công việc',
          duration: 3000
        });

        toast.present();
        // this.navCtrl.push(PayPage, { dt: this.data });
      }, (err) => {
        loader.dismiss();
        alert("update trạng thái không thành công");
        // loader.dismiss();
      });
    }

    if (this.status == "6") {
      for (let a of this.data.content.assignInfo.assignee) {
        a.statusTask = '1';
      };

      this.status = "5";
      let status = "\"5\"";

      let body = {
        "content.status": this.status,
        "content.assignInfo.assignee": this.data.content.assignInfo.assignee

      }

      this.serviceProvider.updateManyField(this.data.id, body).subscribe(data => {

        let newMessage = {
          from: "mobile",
          to: "crm",
          datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
          content: {
            mobileChannel: this.serviceProvider.idOfDevice,
            currentOrderStatus: "5",
            orderID: this.data.id,
            id_incr: this.data.id_incr,
            msg: `Đơn hàng đã hoàn tất công việc, đã thanh toán`
          },
          isNew: "1"
        };

        let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;

        this.serviceProvider.publishMessage(newMessage, id);
        this.serviceProvider.publishMessagetoCustomer(newMessage, id, this.data.content.mobileChannel);

        this.isFinishTask = true;
        let toast = this.toastController.create({
          message: 'Bạn đã hoàn thành công việc',
          duration: 3000
        });

        loader.dismiss();
        toast.present();
        // this.navCtrl.push(PayPage, { dt: this.data });
      }, (err) => {
        loader.dismiss();
        alert("update trạng thái không thành công");
        // loader.dismiss();
      });
    }


  }

  confirmPayment() {
    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ..."
    });
    loader.present();

    // this.navCtrl.pop();

    if (this.status == "3") {
      this.status = "6";
      let status = "\"6\"";

      let paymentInfo = {
        "invoice": {},
        "paymentDate": moment().format("DD/MM/YYYY HH:mm:ss").toString(),
        "paymentMethod": "0",
        "status": "1"
      }

      let body = {
        "content.status": this.status,
        "content.paymentInfo": paymentInfo

      }

      this.serviceProvider.updateManyField(this.data.id, body).subscribe(data => {
        let newMessage = {
          from: "mobile",
          to: "crm",
          datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
          content: {
            mobileChannel: this.serviceProvider.idOfDevice,
            currentOrderStatus: "6",
            orderID: this.data.id,
            id_incr: this.data.id_incr,
            msg: `Đơn hàng đã thanh toán, chưa hoàn tất công việc`
          },
          isNew: "1"
        };

        let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;

        this.serviceProvider.publishMessage(newMessage, id);
        this.serviceProvider.publishMessagetoCustomer(newMessage, id, this.data.content.mobileChannel);

        loader.dismiss();
        this.isPayment = true;
        let toast = this.toastController.create({
          message: 'Bạn đã nhận thanh toán',
          duration: 3000
        });

        toast.present();

        // this.navCtrl.pop();
      }, (err) => {
        loader.dismiss();
        alert("update thanh toán không thành công");
        // loader.dismiss();
      });
    }

    if (this.status == "4") {
      for (let a of this.data.content.assignInfo.assignee) {
        a.statusTask = '1';
      };

      this.status = "5";

      let paymentInfo = {
        "invoice": {},
        "paymentDate": moment().format("DD/MM/YYYY HH:mm:ss").toString(),
        "paymentMethod": "0",
        "status": "1"
      }

      let body = {
        "content.status": this.status,
        "content.paymentInfo": paymentInfo,
        "content.assignInfo.assignee": this.data.content.assignInfo.assignee
      }

      this.serviceProvider.updateManyField(this.data.id, body).subscribe(data => {
        let newMessage = {
          from: "mobile",
          to: "crm",
          datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
          content: {
            mobileChannel: this.serviceProvider.idOfDevice,
            currentOrderStatus: "5",
            orderID: this.data.id,
            id_incr: this.data.id_incr,
            msg: `Đơn hàng đã hoàn tất công việc, đã thanh toán`
          },
          isNew: "1"
        };

        let id = moment().format('DDMMYYYYHHmmss').toString() + this.data.content.id;

        this.serviceProvider.publishMessage(newMessage, id);
        this.serviceProvider.publishMessagetoCustomer(newMessage, id, this.data.content.mobileChannel);

        loader.dismiss();
        this.isPayment = true;
        let toast = this.toastController.create({
          message: 'Bạn đã nhận thanh toán',
          duration: 3000
        });

        toast.present();


        // this.navCtrl.pop();
      }, (err) => {
        loader.dismiss();
        alert("update thanh toán không thành công")
      });
    }


  }

  async preSaveSurvey() {
    let check = 0;
    for (var i = 0; i < this.orderValue.details.length; i++) {
      if (this.orderValue.details[i].charge == undefined || this.orderValue.details[i].charge == "" || this.orderValue.details[i].charge == null) {
        check = 1;
        let toast = this.toastController.create({
          message: 'Bạn phải điền đầy đủ giá tiền dự kiến cho đơn hàng!',
          duration: 2000,
          position: 'top'
        });

        toast.present();
        break;
      }
    }

    if (check == 0) {
      const alert = await this.alertController.create({
        // header: 'Confirm!',
        message: 'Bạn có chắc muốn hoàn thành công việc không?',
        buttons: [
          {
            text: 'Hủy',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Đồng ý',
            handler: () => {
              this.saveSurvey();
            }
          }
        ]
      });

      await alert.present();
    }

  }

  saveSurvey() {
    for (var i = 0; i < this.orderValue.details.length; i++) {
      this.orderValue.details[i].charge = this.orderValue.details[i].charge.toString();
    }

    let data = {
      chosenServices: this.chosenServices,
      imageList: [],
      orderValue: this.orderValue,
      videoList: '',
      note: this.note
    }

    const loader = this.loadingCtrl.create({
      content: "Vui lòng chờ..."
    });
    loader.present();

    let body = {
      "content.status": "0b",
      "content.surveyByTechnicianInfo.content": data,
      "content.surveyByTechnicianInfo.status": "0b"
    }

    this.serviceProvider.updateManyField(this.id, body).subscribe(res => {
      console.log("KQ update status", res);
      this.data.content.status = "0b";

      loader.dismiss();
      // this.navCtrl.pop();

      let newMessage = {
        from: "mobile",
        to: "crm",
        datetime: moment().format('DD/MM/YYYY HH:mm:ss'),
        content: {
          mobileChannel: this.serviceProvider.idOfDevice,
          currentOrderStatus: "0b",
          orderID: this.id,
          id_incr: this.data.id_incr,
          msg: "KTV hoàn thành khảo sát"
        },
        isNew: "1"
      };

      let id = moment().format('DDMMYYYYHHmmss').toString() + this.id;

      this.serviceProvider.publishMessage(newMessage, id);
    }, (err) => {
      console.log(err);
      loader.dismiss();
      // this.navCtrl.pop();
    });

    // this.navCtrl.pop();

    if (this.imageResponse.length > 0)
      this.newImgsSender()
    else if (this.imageList.length > 0) {
      this.serviceProvider.saveImgVideotoOrderSurvey(this.id, this.imageList, null);
    }
    if (this.video != '' && this.video != this.videoOri)
      this.videosSender();
    else if (this.video != '') {
      this.serviceProvider.saveImgVideotoOrderSurvey(this.id, null, this.video);
    }
  }

  txtMaxLengthCheck(e) {
    this.txtcounter = e.length

  }


  checkCharge() {
    // console.log(e.placeholder);
    // console.log(this.orderValue);
    // dv.charge = e.placeholder;
    let a = 0;
    for (var i = 0; i < this.orderValue.details.length; i++) {
      if (this.orderValue.details[i].charge == "" || this.orderValue.details[i].charge == undefined) {
        a += 0;
      }
      else {
        a += +this.orderValue.details[i].charge;
      }
    }
    this.orderValue.sum = a.toString();
  }

  formatNumber(x: string) {
    // let a: number = +v;
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(x))
      x = x.replace(pattern, "$1.$2");
    // console.log(x);
    return x;
  }

  gotoUpdateServices() {
    let popover = this.popoverCtrl.create(UpdateServicesPage, { data: this.chosenServices }, { cssClass: 'updateServices' });
    popover.present();

    popover.onDidDismiss(data => {
      console.log("kien");
      // this.getTaskInProcessOfChild(this.childrenProvider.childData.id);
    })
  }

  changeServices() {
    console.log(this.chosenServices);
    let data = [];
    for (var i = 0; i < this.chosenServices.length; i++) {
      let temp = false;
      for (var j = 0; j < this.orderValue.details.length; j++) {
        if (this.chosenServices[i] == this.orderValue.details[j].id) {
          data.push(this.orderValue.details[j]);
          temp = true;
          break;
        }
      }

      if (temp == false) {
        for (var j = 0; j < this.services.length; j++) {
          if (this.chosenServices[i] == this.services[j].data_item.content.id) {
            data.push(this.services[j].data_item.content);
            break;
          }
        }
      }
    };
    this.orderValue.details = data;
    this.checkCharge();
  }

  fixDisplayURLFromFileURI(url: string) {
    let win: any = window;
    return win.Ionic.WebView.convertFileSrc(url)
  }

  async presentActionPhoto() {
    const actionSheet = await this.actionSheetController.create({
      // header: 'Albums',
      buttons: [{
        text: 'Chụp ảnh mới',
        icon: 'camera',
        handler: () => {
          console.log('Delete clicked');
          this.takePicture();
        }
      },
      {
        text: 'Lấy từ thư viện',
        icon: 'images',
        handler: () => {
          console.log('Share clicked');
          this.getImages();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
      ]
    });
    await actionSheet.present();
  }

  async presentActionVideo() {
    const actionSheet = await this.actionSheetController.create({
      // header: 'Albums',
      buttons: [{
        text: 'Quay mới',
        icon: 'videocam',
        handler: () => {
          console.log('Delete clicked');
          this.recordVideo();
        }
      },
      {
        text: 'Lấy từ thư viện',
        icon: 'film',
        handler: () => {
          console.log('Share clicked');
          this.getVideo();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
      ]
    });
    await actionSheet.present();
  }

  takePicture() {
    let options: CameraOptions = {
      quality: 0,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    };
    if (Array.isArray(this.imageResponse)) {
      if (this.imageResponse.length + this.imageList.length == 10) {
        alert(
          "Bạn chỉ được gửi tối đa 10 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
        );
      } else {
        this.camera.getPicture(options).then(
          imageData => {
            console.log('file uri: ' + imageData)
            this.imageResponse.push(imageData)
          },
          err => {
            console.log(err);
          }
        );
      }
    }
    else {
      this.camera.getPicture(options).then(
        imageData => {
          console.log('file uri: ' + imageData)
        },
        err => {
          console.log(err);
        }
      );
    }

  }

  recordVideo() {
    if (this.video != "")
      alert(
        "Bạn chỉ được gửi tối đa 1 video trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    else {
      let options: CaptureVideoOptions = { limit: 1, quality: 0 };
      this.mediaCapture.captureVideo(options).then(
        (data: MediaFile[]) => {
          console.log(data);
          var i, path, len;
          for (i = 0, len = data.length; i < len; i += 1) {
            this.video = data[i].fullPath;
            console.log(this.video)
          }
        },
        (err: CaptureError) => {
          console.error(err);
        }
      );
    }
  }

  getImages() {
    if (this.imageResponse.length + this.imageList.length == 10) {
      alert(
        "Bạn chỉ được gửi tối đa 10 tấm ảnh trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    } else {
      let count = 10 - (this.imageResponse.length + this.imageList.length);
      let options: ImagePickerOptions = {
        quality: 0,
        outputType: 0,
        maximumImagesCount: count
      };
      this.imagePicker.getPictures(options).then(
        results => {
          for (let pic of results) {
            if (pic !== 'O' && pic !== 'K')
              this.imageResponse.push(pic);
          }
          console.log(this.imageResponse)
        },
        err => {
          console.log(err)
        }
      );
    }
  }

  delPic(pic, index) {
    console.log('ảnh cần xoá: ' + pic)
    this.imageResponse.splice(index, 1);
  }

  delPic1(pic, index) {
    console.log('ảnh cần xoá: ' + pic)
    this.imageList.splice(index, 1);
  }

  getVideo() {
    console.log("video");
    const options: CameraOptions = {
      quality: 50,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.FILE_URI,
      mediaType: this.camera.MediaType.VIDEO
    };

    if (this.video != "")
      alert(
        "Bạn chỉ được gửi tối đa 1 video trong một lần gửi, nếu muốn thay đổi, hãy bỏ bớt những tệp đang chọn"
      );
    else {
      this.camera.getPicture(options).then((data) => {
        console.log(data)
        this.video = data
      }, (err) => {
        console.log(err)
      });
    }


  }

  delVideo() {
    this.video = ''
  }

  newImgsSender() {
    for (let i = 0; i < this.imageResponse.length; ++i) {
      let pic = this.imageResponse[i]
      console.log('link ảnh:' + pic);
      if (pic.startsWith("file")) {
        this.file
          .resolveLocalFilesystemUrl(pic)
          .then(entry => {
            (entry as FileEntry).file(file => this.readnewImgFile(file, i));
          })
          .catch(err => console.log(err));
      } else {
        this.file
          .resolveLocalFilesystemUrl("file://" + pic)
          .then(entry => {
            (entry as FileEntry).file(file => this.readnewImgFile(file, i));
          })
          .catch(err => console.log(err));
      }

    }
  }

  private readnewImgFile(file: any, index) {
    let filetype = file.type.split('/')[1]
    console.log(filetype)
    const reader = new FileReader();
    reader.onloadend = () => {
      const imgBlob = new Blob([reader.result], { type: file.type });
      console.log('append to form')
      this.masterFormdataToSend.append("data", imgBlob, Math.abs(new Date().getTime()).toString() + '.' + filetype)
      if (index == this.imageResponse.length - 1) {
        let self = this;
        setTimeout(function () {
          console.log("Timeout");
          self.postNewImgData(self.masterFormdataToSend, 'abc')
        }, 3000);

      }
    };
    reader.readAsArrayBuffer(file);
  }

  private postNewImgData(formData: FormData, filename) {
    console.log('tên file: ' + filename);
    console.log("Here and ready to be uploaded - new img upload");
    console.log(formData.getAll('data'))

    let url = this.serviceProvider.apiURL.uploadFile
    console.log(url)
    let headers = new Headers({
      enctype: "multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA",
      // userid: "user1",
      // deviceid: "crm_app",
      Authorization: `Bearer ${this.serviceProvider.token}`
    });
    let options = new RequestOptions({ headers: headers });
    this.http
      .post(url, formData, options)
      .subscribe(
        data => {
          console.log(data)
          console.log('gửi ảnh thành công');
          let a = <any>data
          console.log(JSON.parse(a._body))
          let arrayTosend = []
          for (let img of JSON.parse(a._body)) {
            console.log(img.path);
            arrayTosend.push(img.path)
          }
          if (arrayTosend.length > 0) {
            let imgList = arrayTosend.concat(this.imageList);
            this.serviceProvider.saveImgVideotoOrderSurvey(this.id, imgList, null)
          }

        },
        err => {
          console.log('gửi ảnh fail');
          console.log(err)
        }
      );
  }

  videosSender() {

    if (this.video.startsWith("file")) {
      this.file
        .resolveLocalFilesystemUrl(this.video)
        .then(entry => {
          (entry as FileEntry).getMetadata(metadata => {
            console.log("video size : " + metadata.size);
            (entry as FileEntry).file(file => this.readFile(file));
          });
        })
        .catch(err => console.log(err));
    } else {
      this.file
        .resolveLocalFilesystemUrl("file://" + this.video)
        .then(entry => {
          (entry as FileEntry).getMetadata(metadata => {
            (entry as FileEntry).file(file => this.readFile(file));
          });
        })
        .catch(err => console.log(err));
    }
  }

  private readFile(file: any) {
    let filetype = file.type.split('/')[1]
    console.log(filetype)
    const reader = new FileReader();
    reader.onloadend = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], { type: file.type });
      formData.append("data", imgBlob, Math.abs(new Date().getTime()).toString() + '.' + filetype);
      this.postData(formData, file.name);
    };
    reader.readAsArrayBuffer(file);
  }

  private postData(formData: FormData, filename) {
    console.log("Here and ready to be uploaded");
    let url = this.serviceProvider.apiURL.uploadFile
    console.log(url)
    let headers = new Headers({
      enctype: "multipart/form-data; boundary=----WebKitFormBoundaryuL67FWkv1CA",
      // userid: "user1",
      // deviceid: "crm_app",
      Authorization: `Bearer ${this.serviceProvider.token}`
    });
    let options = new RequestOptions({ headers: headers });
    this.http
      .post(url, formData, options)
      .subscribe(
        data => {
          console.log(data)
          console.log('gửi video thành công');
          let a = <any>data
          console.log(JSON.parse(a._body))
          let arrayTosend = []
          for (let img of JSON.parse(a._body)) {
            console.log(img.path);
            arrayTosend.push(img.path)
          }
          if (arrayTosend.length > 0)
            this.serviceProvider.saveImgVideotoOrderSurvey(this.id, null, arrayTosend[0])
        },
        err => {
          console.log('gửi video fail')
          console.log(err)
        }
      );
  }
}
