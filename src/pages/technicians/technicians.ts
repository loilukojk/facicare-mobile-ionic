import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import moment from 'moment';
import { DetailTaskPage } from '../detail-task/detail-task';
import { ServiceProvider } from '../../services/GlobalVar';

@Component({
  selector: 'page-technicians',
  templateUrl: 'technicians.html',
})
export class TechniciansPage {
  tasks: any = [];
  SurveyTasks: any = [];
  currDate = moment().format("MM/DD/YYYY");
  todaytasks: any;
  checkNull = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modal: ModalController,
    public loadingCtrl: LoadingController, public serviceProvider: ServiceProvider) {

  }

  ionViewWillEnter() {
    this.serviceProvider.getAPIKey().subscribe(res => {
      let result = <any>res;
      if (result.data_item != undefined) {
        this.serviceProvider.token = result.data_item.apiKey;
        // console.log("APIKey: ", this.serviceProvider.token);
      }
    });
    this.tasks = [];
    this.SurveyTasks = [];
    this.getData();
  }

  getData() {
    console.log("KDKKDKDK")
    if (this.serviceProvider.currentUser != '') {

      const loader = this.loadingCtrl.create({
        content: "Vui lòng chờ..."
      });
      loader.present();

      this.serviceProvider.getTodayTask().subscribe((data) => {
        loader.dismiss();
        let res = <any>data;
        if (res.length == undefined) {
          this.checkNull = true;
        }
        else {
          this.tasks = res
          // .sort((a, b) => {
          //   let t1 = a.created_at;
          //   let t2 = b.created_at;
    
          //   if (t1 > t2) {
          //     return 1;
          //   }
          //   if (t1 < t2) {
          //     return -1;
          //   }
    
          //   return 0;
          //   // return Number(a.data_item.content.index) < Number(b.data_item.content.index)
          // });
          console.log(res);
          this.checkNull = false;
        }

        this.serviceProvider.getTodaySurveyTask().subscribe((data) => {
          // console.log("data", data);
          let res = <any>data;
          if (res.length > 0) {
            this.SurveyTasks = res
            // .sort((a, b) => {
            //   let t1 = a.created_at;
            //   let t2 = b.created_at;
      
            //   if (t1 > t2) {
            //     return 1;
            //   }
            //   if (t1 < t2) {
            //     return -1;
            //   }
      
            //   return 0;
            //   // return Number(a.data_item.content.index) < Number(b.data_item.content.index)
            // });
            console.log(res);
            // Array.prototype.push.apply(this.tasks, res);
            // console.log(this.tasks);
          }
        })

      }, (res) => {
        alert("Lỗi hệ thống! Vui lòng thử lại sau");
        loader.dismiss();
      })
    }
    else {
      this.checkNull = true;
      this.tasks = [];
    }
  }

  toServicesPrice(v: number) {
    var data = this.tasks[v];
    console.log(data);
    this.navCtrl.push(DetailTaskPage, { dt: data });

    console.log("dfgd", data)
  }

  toServicesPrice_Survey(v: number) {
    var data = this.SurveyTasks[v];
    console.log(data);
    this.navCtrl.push(DetailTaskPage, { dt: data, isSurveyTask: true });

    console.log("dfgd", data)
  }

  check(assignee, status) {
    if (status == '5') {
      return 1;
    }
    for (let a of assignee) {
      if (a.id == this.serviceProvider.currentUser) {
        if (a.statusTask == '1') {
          return 0;
        }
        else {
          return 1;
        }
      }
    }
  }

}
