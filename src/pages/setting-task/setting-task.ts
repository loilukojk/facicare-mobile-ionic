import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-setting-task',
  templateUrl: 'setting-task.html',
})
export class SettingTaskPage {
  data;

  constructor(public viewCtrl: ViewController, public localStorage: Storage, public alertController: AlertController, public navCtrl: NavController, public navParams: NavParams) {
    this.data = this.navParams.get('data');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingTaskPage');
  }

  async delete() {
    
  }

}
