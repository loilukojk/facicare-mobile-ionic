import { Injectable } from '@angular/core';
import { Component, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Nav, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import moment from 'moment';
import { Http, Headers } from '@angular/http';
import sdk from '../sdk/index';
import { AccountPage } from '../pages/account/account';

@Injectable()
export class ServiceProvider {
    @ViewChild(Nav) nav: Nav;

    apiURL = {
        'uploadFile': 'https://sme-dev.vdc2.com.vn/api/core/v1/minio/upload/miniobucket1'
    }

    selectedServiceList = [];
    orders: any = JSON.parse(localStorage.getItem('BookData')) || [];
    services: any;
    isLogin = false;
    currentUser = "";
    environment = "https://sme-dev.vdc2.com.vn";
    // environment = "http://221.132.29.81:1325";
    environment1 = "http://221.132.29.81:9990";
    // environment="http://203.162.141.98:1323";
    userid = "user1";
    deviceid = "crm_app";
    token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkZXZpY2VJRCI6ImNybV9hcHAiLCJleHAiOjE2MjM1NDU0NzIsInVzZXJJRCI6InVzZXIxIn0.LCHlhdlUvx59hxt1kGTby5fPJn5KkiwifpobXgbHedo';
    idOfDevice;
    isCustomer = true;

    constructor(public alertController: AlertController, public storage: Storage, private http: HttpClient, private newHTTP: Http) {
    }

    getServices() {
        let SERVER_URL = `${this.environment}/api/core/v1/data/all_in_bucket/crm_app_service`;
        let headers = {
            Authorization: `Bearer ${this.token}`,
            // userid: this.userid,
            // deviceid: this.deviceid
        }
        return this.http.get(SERVER_URL, { headers: headers })
    }

    getAPIKey(){
        let SERVER_URL = `${this.environment}/api/core/v1/device/detail/crm_app`;
        let headers = {
            Authorization: `Bearer ${this.token}`,
            userid: this.userid,
            // deviceid: this.deviceid
        }
        return this.http.get(SERVER_URL, { headers: headers })
    }

    getTechnician() {
        let SERVER_URL = `${this.environment}/api/core/v1/data/all_in_bucket/crm_app_technician`;
        let headers = {
            Authorization: `Bearer ${this.token}`,
            // userid: this.userid,
            // deviceid: this.deviceid
        }

        return this.http.get(SERVER_URL, { headers: headers })
    }

    createOrder(orderID: string, body: any) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/create/crm_app_order/${orderID}`;
        let headers = {
            // userid: this.userid,
            // deviceid: this.deviceid,
            Authorization: `Bearer ${this.token}`
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    updateOrder(orderID: string, status: string) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/update_one/crm_app_order/${orderID}/content/status`;
        let headers = {
            // userid: this.userid,
            // deviceid: this.deviceid,
            Authorization: `Bearer ${this.token}`
        }

        return this.http.patch(SERVER_URL, status, { headers: headers });
    }

    updateOrderPayment(orderID: string, body: any) {
        console.log({
            orderID: orderID,
            paymentInfo: body
        })
        let SERVER_URL = `${this.environment}/api/core/v1/data/update_one/crm_app_order/${orderID}/content/paymentInfo`;
        let headers = {
            // userid: this.userid,
            // deviceid: this.deviceid,
            Authorization: `Bearer ${this.token}`,
            'Content-Type': 'application/json'
        }

        return this.http.patch(SERVER_URL, body, { headers: headers });
    }

    updateOrderNote(orderID: string, note: string) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/update_one/crm_app_order/${orderID}/content/note`;
        let headers = {
            // userid: this.userid,
            // deviceid: this.deviceid,
            Authorization: `Bearer ${this.token}`
        }

        return this.http.patch(SERVER_URL, `\"${note}\"`, { headers: headers });
    }

    updateOrderPriceQuoteTimes(orderID: string, priceQuoteTimes: string) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/update_one/crm_app_order/${orderID}/content/priceQuoteTimes`;
        let headers = {
            // userid: this.userid,
            // deviceid: this.deviceid,
            Authorization: `Bearer ${this.token}`
        }

        return this.http.patch(SERVER_URL, `\"${priceQuoteTimes}\"`, { headers: headers });
    }

    updateOrderPriceQuoteRequests(orderID: string, note) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/update_one/crm_app_order/${orderID}/content/priceQuoteRequests`;
        let headers = {
            // userid: this.userid,
            // deviceid: this.deviceid,
            Authorization: `Bearer ${this.token}`
        }

        return this.http.patch(SERVER_URL, note, { headers: headers });
    }

    updateContentOfSurveyByTechnicianInfo(orderID, data) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/update_one/crm_app_order/${orderID}/content/surveyByTechnicianInfo/content`;
        let headers = {
            // userid: this.userid,
            // deviceid: this.deviceid,
            Authorization: `Bearer ${this.token}`
        }

        return this.http.patch(SERVER_URL, data, { headers: headers });
    }

    updateOrderContract(orderID: string, contractType: string) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/update_one/crm_app_order/${orderID}/content/hasContract`;
        let headers = {
            // userid: this.userid,
            // deviceid: this.deviceid,
            Authorization: `Bearer ${this.token}`
        }

        return this.http.patch(SERVER_URL, contractType, { headers: headers });
    }

    getCompanyInfo() {
        let SERVER_URL = `${this.environment}/api/core/v1/data/detail/crm_app_company/company01`;
        let headers = {
            // userid: this.userid,
            // deviceid: this.deviceid,
            Authorization: `Bearer ${this.token}`
        }

        return this.http.get(SERVER_URL, { headers: headers });
    }

    getOrder(orderID: any) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/detail/crm_app_order/${orderID}`;
        let headers = {
            // userid: this.userid,
            // deviceid: this.deviceid,
            Authorization: `Bearer ${this.token}`
        }
        return this.http.get(SERVER_URL, { headers: headers });
    }

    getGenerateID(body) {
        let SERVER_URL = `${this.environment}/api/safehouse/v1/order/generate_id`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    editNotification(orderID: string, status: string) {
        this.storage.get("notificationHistory").then((res) => {
            let data = JSON.parse(res);
            for (let i = 0; i < data.length; i++) {
                if (data[i].order.id == orderID) {
                    data[i].order.content.status = status;
                    break;
                }
            }
            this.storage.set("notificationHistory", JSON.stringify(data));
        })
    }

    saveImgVideotoOrder(orderID, imgArray: any, video: any) {
        console.log(
            {
                orderID: orderID,
                imgList: imgArray,
                video: video
            }
        )
        let SERVER_URL = `${this.environment}/api/core/v1/data/update_one/crm_app_order/${orderID}/content/videoList`;
        let SERVER_URL1 = `${this.environment}/api/core/v1/data/update_one/crm_app_order/${orderID}/content/imageList`;
        let headers = {
            // userid: this.userid,
            // deviceid: this.deviceid,
            Authorization: `Bearer ${this.token}`
        }
        if (video != null)
            this.http.patch(SERVER_URL, `\"${video}\"`, { headers: headers }).subscribe(
                (res) => console.log(res),
                (err) => console.log(err),
                () => console.log('done!')
            );

        if (imgArray != null)
            this.http.patch(SERVER_URL1, imgArray, { headers: headers }).subscribe(
                (res) => console.log(res),
                (err) => console.log(err),
                () => console.log('done!')
            );

    }

    saveImgVideotoOrderSurvey(orderID, imgArray: any, video: any) {
        console.log(
            {
                orderID: orderID,
                imgList: imgArray,
                video: video
            }
        )
        let SERVER_URL = `${this.environment}/api/core/v1/data/update_one/crm_app_order/${orderID}/content/surveyByTechnicianInfo/content/videoList`;
        let SERVER_URL1 = `${this.environment}/api/core/v1/data/update_one/crm_app_order/${orderID}/content/surveyByTechnicianInfo/content/imageList`;
        let headers = {
            // userid: this.userid,
            // deviceid: this.deviceid,
            Authorization: `Bearer ${this.token}`
        }
        if (video != null)
            this.http.patch(SERVER_URL, `\"${video}\"`, { headers: headers }).subscribe(
                (res) => console.log(res),
                (err) => console.log(err),
                () => console.log('done!')
            );

        if (imgArray != null)
            this.http.patch(SERVER_URL1, imgArray, { headers: headers }).subscribe(
                (res) => console.log(res),
                (err) => console.log(err),
                () => console.log('done!')
            );

    }

    getTodayTask() {
        let SERVER_URL = `${this.environment}/api/core/v1/data/all_in_bucket/match/crm_app_order`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        let body =
        {
            "content.assignInfo.assigneeIDList": this.currentUser,
            "content.assignInfo.workDate": moment().format("DD/MM/YYYY")
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    getMonthTask() {
        let SERVER_URL = `${this.environment}/api/core/v1/data/all_in_bucket/match/crm_app_order`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        let body =
        {
            "content.assignInfo.assigneeIDList": this.currentUser,
            "content.assignInfo.workDate": moment().format("MM/YYYY")
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    getNextMonthTask() {
        let SERVER_URL = `${this.environment}/api/core/v1/data/all_in_bucket/match/crm_app_order`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        let body =
        {
            "content.assignInfo.assigneeIDList": this.currentUser,
            "content.assignInfo.workDate": moment().add(1, 'months').format("MM/YYYY")
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    getDayTask(date: string) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/all_in_bucket/match/crm_app_order`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        let body =
        {
            "content.assignInfo.assigneeIDList": this.currentUser,
            "content.assignInfo.workDate": moment(date).format("DD/MM/YYYY")
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    getHistoryTask() {
        let SERVER_URL = `${this.environment}/api/core/v1/data/all_in_bucket/match/crm_app_order`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        let body =
        {
            "content.assignInfo.assigneeIDList": this.currentUser,
            "content.status": "5"
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    getTodaySurveyTask() {
        let SERVER_URL = `${this.environment}/api/core/v1/data/all_in_bucket/match/crm_app_order`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        let body =
        {
            "content.surveyByTechnicianInfo.assignee.id": this.currentUser,
            "content.surveyByTechnicianInfo.workDate": moment().format("DD/MM/YYYY")
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    getMonthSurveyTask() {
        let SERVER_URL = `${this.environment}/api/core/v1/data/all_in_bucket/match/crm_app_order`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        let body =
        {
            "content.surveyByTechnicianInfo.assignee.id": this.currentUser,
            "content.surveyByTechnicianInfo.workDate": moment().format("MM/YYYY")
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    getNextMonthSurveyTask() {
        let SERVER_URL = `${this.environment}/api/core/v1/data/all_in_bucket/match/crm_app_order`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        let body =
        {
            "content.surveyByTechnicianInfo.assignee.id": this.currentUser,
            "content.surveyByTechnicianInfo.workDate": moment().add(1, 'months').format("MM/YYYY")
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    getDaySurveyTask(date: string) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/all_in_bucket/match/crm_app_order`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        let body =
        {
            "content.surveyByTechnicianInfo.assignee.id": this.currentUser,
            "content.surveyByTechnicianInfo.workDate": moment(date).format("DD/MM/YYYY")
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    getHistorySurveyTask() {
        let SERVER_URL = `${this.environment}/api/core/v1/data/all_in_bucket/match/crm_app_order`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        let body =
        {
            "content.surveyByTechnicianInfo.assignee.id": this.currentUser,
            "content.surveyByTechnicianInfo.status": "0b"
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    updatePaymentStatus(orderID: string, status: string) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/update_one/crm_app_order/${orderID}/content/paymentInfo/status`;
        let headers = {
            // userid: this.userid,
            // deviceid: this.deviceid,
            Authorization: `Bearer ${this.token}`
        }

        return this.http.patch(SERVER_URL, status, { headers: headers });
    }


    getProvinces() {
        let SERVER_URL = `${this.environment}/api/core/v1/data/detail/crm_app_donvihanhchinh/tree_query/content/province_list`;
        let headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.token);

        return this.newHTTP.get(SERVER_URL, { headers: headers });
    }

    getDistrictsInProvince(provincesID) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/detail/crm_app_donvihanhchinh/tree_query/content/"${provincesID}"/district_list`
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        return this.http.get(SERVER_URL, { headers: headers });
    }

    getWardsInDistrict(provincesID, districtID) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/detail/crm_app_donvihanhchinh/tree_query/content/"${provincesID}"/"${districtID}"/ward_list`
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        return this.http.get(SERVER_URL, { headers: headers });
    }

    createHistoryOrder(id, content) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/create/crm_app_push_notification/${id}`;
        let headers = {
            // userid: this.userid,
            // deviceid: this.deviceid,
            Authorization: `Bearer ${this.token}`
        };

        let body = {
            createdByDevice: "crm_app",
            bucket: "crm_app_push_notification",
            creatingDate: moment().format('DD/MM/YYYY HH:mm:ss').toString(),
            description: "a push notification record",
            content: content
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    publishMessage(message, idNotification) {
        // this.sdk.notification.Connect();
        const crmChannel = "CRM_App";

        let newMessage = message;
        let id = idNotification;

        console.log("testNOTI", sdk.notification);
        this.createHistoryOrder(id, newMessage).subscribe(res => {
            sdk.notification.PublishToChannel(crmChannel, newMessage).then(res => {
                console.log(res);
                //   alert('Push noti thành công')
                // Vô được đây là push thành công
                // some code here (nếu cần)
                console.log("Push notification successfully")
            }, function (err) {
                // alert('Push noti thất bại')
                // Vô đây tức là push KHÔNG thành công
                // some code here (nếu cần)
                console.log("Push notification failed")
            });
            console.log("đã gửi thông báo thành công", res);
            // alert('gửi lịch sử thành công');
        });
    }

    publishMessagetoCustomer(message, idNotification, mobileChannel) {
        // this.sdk.notification.Connect();
        //   alert(this.idOfDevice);
        const crmChannel = mobileChannel;

        let newMessage = message;
        let id = idNotification;

        console.log("testNOTI", sdk.notification);
        sdk.notification.PublishToChannel(crmChannel, newMessage).then(res => {
            console.log(res);
            //   alert('Push noti thành công')
            // Vô được đây là push thành công
            // some code here (nếu cần)
            console.log("Push notification successfully")
        }, function (err) {
            // alert('Push noti thất bại')
            // Vô đây tức là push KHÔNG thành công
            // some code here (nếu cần)
            console.log("Push notification failed")
        });
    }

    login(body) {
        let SERVER_URL = `${this.environment}/api/core/v1/user/login/safehouse`;

        let headers = {
            Authorization: `Bearer ${this.token}`
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    async checkExpirationTime() {
        let time = this.storage.get("ExpireationTime").then(data => {
            if (data != null || data != undefined) {
                if (moment(data) < moment()) {
                    const alert = this.alertController.create({
                        // header: 'Confirm!',
                        message: 'Bạn cần đăng nhập lại để tiếp tục!',
                        buttons: [
                            {
                                text: 'Đồng ý',
                                handler: () => {
                                    this.storage.remove("Account");
                                    this.currentUser = '';
                                    this.isLogin = false;
                                    // this.nav.push(AccountPage,{});
                                    // this.navCtrl.popToRoot();
                                }
                            }
                        ]
                    });

                    alert.present();
                }
            }
        })
    }

    register(body) {
        let SERVER_URL = `${this.environment}/api/core/v1/user/register/safehouse`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    getBackLogTask() {
        let SERVER_URL = `${this.environment}/api/core/v1/data/all_in_bucket/match/crm_app_order`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        let body =
        {
            "content.status": "3",
            "content.assignInfo.assigneeIDList": this.currentUser,
            "content.assignInfo.deadline": `$lte.${moment().format("DD/MM/YYYY")} 00:00:00`
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    getBackLogSurveyTask() {
        let SERVER_URL = `${this.environment}/api/core/v1/data/all_in_bucket/match/crm_app_order`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }
        let body =
        {
            "content.status": "0a",
            "content.surveyByTechnicianInfo.assignee.phone": this.currentUser,
            "content.surveyByTechnicianInfo.deadline": `$lte.${moment().format("DD/MM/YYYY")} 00:00:00`
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    updateAssignee(orderID: string, assignee: any) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/update_one/crm_app_order/${orderID}/content/assignInfo/assignee`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }

        return this.http.patch(SERVER_URL, assignee, { headers: headers });
    }

    createAccount(id: string, body){
        let SERVER_URL = `${this.environment}/api/core/v1/data/create/crm_app_customer/${id}`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }

        return this.http.post(SERVER_URL, body, { headers: headers });
    }

    getCustomerInfo(id:string){
        let SERVER_URL = `${this.environment}/api/core/v1/data/detail/crm_app_customer/${id}`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }

        return this.http.get(SERVER_URL, { headers: headers });
    }

    updateManyField(id: string, body) {
        let SERVER_URL = `${this.environment}/api/core/v1/data/update_field/crm_app_order/${id}`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }

        return this.http.patch(SERVER_URL, body, { headers: headers });
    }

    getTrackingOrder(orderID: string) {
        let SERVER_URL = `${this.environment}/api/core/v1/event/data/crm_app_order/${orderID}`;
        let headers = {
            Authorization: `Bearer ${this.token}`
        }

        return this.http.get(SERVER_URL, { headers: headers });
    }
}