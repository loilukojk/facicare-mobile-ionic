import { Component, ViewChild } from '@angular/core';
import { MenuController, Nav, Slides } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { NavParams, ModalController, AlertController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { AccountPage } from '../pages/account/account';
import { TechniciansPage } from '../pages/technicians/technicians';
import { Tab2Page } from '../pages/tab2/tab2';
import { ServicesPricePage } from '../pages/services-price/services-price';
import { BookPage } from '../pages/book/book';
import { ServiceProvider } from '../services/GlobalVar';
import sdk from '../sdk/index';
import moment from 'moment';
import { Badge } from '@ionic-native/badge';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  @ViewChild(Slides) slides: Slides;
  rootPage: any = TabsPage;
  pages: Array<{ title: string, component: any, icon: string }>;

  constructor(public localStorage: Storage, public menu: MenuController, statusBar: StatusBar,
    splashScreen: SplashScreen, private push: Push, public serviceProvider: ServiceProvider,
    public platform: Platform, public alertCtrl: AlertController, private badge: Badge) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.initPushNotification();
      this.badge.requestPermission().then(res => {
        this.badge.set(10);
      });
      this.localStorage.get("Account").then(res => {
        console.log(res);
        if (res != null && res.status == 1) {
          this.serviceProvider.currentUser = res.username;
          this.serviceProvider.isLogin = true;
          if (res.roles == "technician") {
            this.nav.setRoot(Tab2Page);
            this.nav.popToRoot();
            this.serviceProvider.isCustomer = false;
          }
          if (res.roles == "end_user") {
            this.nav.setRoot(TabsPage);
            this.nav.popToRoot();
            this.serviceProvider.isCustomer = true;
          }
          // this.nav.setRoot(Tab2Page);
        }
      })

      localStorage.get('notificationHistory').then((val) => {
        if (val == null) {
          localStorage.set('notificationHistory', JSON.stringify([]));
        }
      });

      this.platform.registerBackButtonAction(() => {

        let view = this.nav.getActive();
        let currentRootPage = view.component;

        if (currentRootPage == BookPage) {
          this.slides.slidePrev();
        }
        else {
          this.nav.pop();
        }

      });
    });

    this.pages = [
      { title: 'Đặt lịch', component: TabsPage, icon: 'clipboard' },
      { title: 'Đăng nhập', component: AccountPage, icon: 'contact' },
      { title: 'Kỹ thuật viên', component: Tab2Page, icon: 'briefcase' }
    ]
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
    // this.check = MapPage.information.length; 
  }

  initPushNotification() {
    this.push.hasPermission()
      .then((res: any) => {
        if (res.isEnabled) {
          console.log('We have permission to send push notifications');
        } else {
          console.log('We don\'t have permission to send push notifications');
        }
      });
    const options: PushOptions = {
      android: {
        senderID: '189805546198',
        iconColor: '#1E88E5',
        icon: 'ic_stat_20100515080508vnpt_logo',
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      },
      windows: {}
    };
    const pushObject: PushObject = this.push.init(options);
    let self = this;


    pushObject.on('notification').subscribe(async (data: any) => {

      console.log('push đến:' + data)
      //Save coming notification to storage 
      let notificationHistory = [];
      self.localStorage.get('notificationHistory').then((val) => {
        notificationHistory = JSON.parse(val);
        console.log("saved noti")
        console.log(notificationHistory)
        var currentdate = new Date();
        var datetime = currentdate.getDate() + "/"
          + (currentdate.getMonth() + 1) + "/"
          + currentdate.getFullYear() + " -- "
          + currentdate.getHours() + ":"
          + currentdate.getMinutes() + ":"
          + currentdate.getSeconds();
        notificationHistory.push({ "title": data.title, "body": data.message, "order": data.additionalData.order, "time": datetime });
        self.localStorage.set("notificationHistory", JSON.stringify(notificationHistory))
      });

      data.datetime = moment().format("DD/MM/YYYY HH:mm:ss");

      let dt = JSON.parse(localStorage.getItem("Notification_History")) || [];

      if (data.additionalData.action == "billing") {
        dt.push({
          datetime: data.datetime,
          orderID: data.additionalData.order.id,
          message: data.message,
          isNew: '1'
        });
      }
      else {
        dt.push({
          datetime: data.datetime,
          orderID: data.additionalData.order.id,
          message: data.message,
          isNew: '1'
        });
      }


      localStorage.setItem("Notification_History", JSON.stringify(dt));

      let numberNoti: number = 0;

      let noti = await JSON.parse(localStorage.getItem("Notification_History")) || [];
      for (let a of noti) {
        if (a.isNew == '1') {
          numberNoti += 1;
        }
      }

      this.badge.set(numberNoti);
      // pushObject.setApplicationIconBadgeNumber(numberNoti);

      if (data.additionalData.action == "billing") {
        if (data.additionalData.foreground) {
          console.log('foreground')

          // if application open, show popup
          let view = this.nav.getActive();

          let confirmAlert = this.alertCtrl.create({
            title: data.title,
            message: data.message,
            buttons: [{
              text: 'Bỏ qua',
              role: 'cancel'
            }, {
              text: 'Đánh giá',
              handler: () => {
                let view = this.nav.getActive();
                this.serviceProvider.getOrder(data.additionalData.order.id).subscribe(res => {
                  let result = <any>res;
                  if (result.data_item != undefined) {
                    this.nav.push(ServicesPricePage, { dt: result.data_item });
                    let noti = JSON.parse(localStorage.getItem("Notification_History"));
                    for (let a of noti) {
                      if (a.message == data.message && a.datetime == data.datetime && a.orderID == data.additionalData.order.id) {
                        a.isNew = '0';
                        localStorage.setItem("Notification_History", JSON.stringify(noti));
                        break;
                      }
                    }
                  }
                })
              }
            }]
          });
          confirmAlert.present();
        } else {
          console.log('background')
          this.serviceProvider.getOrder(data.additionalData.order.id).subscribe(res => {
            let result = <any>res;
            if (result.data_item != undefined) {
              this.nav.push(ServicesPricePage, { dt: result.data_item });
              let noti = JSON.parse(localStorage.getItem("Notification_History"));
              for (let a of noti) {
                if (a.message == data.message && a.datetime == data.datetime && a.orderID == data.additionalData.order.id) {
                  a.isNew = '0';
                  localStorage.setItem("Notification_History", JSON.stringify(noti));
                  break;
                }
              }
            }
          })
        }
      }
      else {
        if (data.additionalData.foreground) {
          console.log('foreground')

          // if application open, show popup
          let view = this.nav.getActive();

          let confirmAlert = this.alertCtrl.create({
            title: data.title,
            message: data.message,
            buttons: [{
              text: 'Bỏ qua',
              role: 'cancel'
            },
            {
              text: 'Đánh giá',
              handler: () => {
                let view = this.nav.getActive();
                this.serviceProvider.getOrder(data.additionalData.order.id).subscribe(res => {
                  let result = <any>res;
                  if (result.data_item != undefined) {
                    this.nav.push(ServicesPricePage, { dt: result.data_item });
                    let noti = JSON.parse(localStorage.getItem("Notification_History"));
                    for (let a of noti) {
                      if (a.message == data.message && a.datetime == data.datetime && a.orderID == data.additionalData.order.id) {
                        a.isNew = '0';
                        localStorage.setItem("Notification_History", JSON.stringify(noti));
                        break;
                      }
                    }
                  }
                })
              }
            }]
          });
          confirmAlert.present();
        } else {
          console.log('background')
          this.serviceProvider.getOrder(data.additionalData.order.id).subscribe(res => {
            let result = <any>res;
            if (result.data_item != undefined) {
              this.nav.push(ServicesPricePage, { dt: result.data_item });
              let noti = JSON.parse(localStorage.getItem("Notification_History"));
              for (let a of noti) {
                if (a.message == data.message && a.datetime == data.datetime && a.orderID == data.additionalData.order.id) {
                  a.isNew = '0';
                  localStorage.setItem("Notification_History", JSON.stringify(noti));
                  break;
                }
              }
            }
          })
        }
      }

      pushObject.finish().then(() => {
        console.log('processing of push data is finished');
      }).catch((err: any) => {
        console.log(err);
      });

    });



    pushObject.on('registration')
      .subscribe((registration: any) => {
        if (registration.registrationId == null)
          this.localStorage.set("DeviceToken", "fakeDeviceID");
        else {
          let deviceToken = registration.registrationId + ";" + "ANDROID";
          this.localStorage.set("DeviceToken", deviceToken);
          this.serviceProvider.idOfDevice = this.formatDeviceID(registration.registrationId);
          this.localStorage.set("MobileChannel", this.formatDeviceID(registration.registrationId));
          this.connectToPushNotificationServer(registration.registrationId);
          // this.authen.currentToken = registration.registrationId
        }
        console.log('Device registered', registration)
        let topic = "pccc_android"
        pushObject.subscribe(topic).then((res: any) => {
          console.log("Success");
        }).catch((error: any) => {
          console.log("Failed");
        })
      });

    pushObject.on('error').
      subscribe(error =>
        console.error('Error with Push plugin', error));
  }

  formatDeviceID(deviceToken) {
    deviceToken = deviceToken.split(":").join("");
    deviceToken = deviceToken.split("$").join("");
    deviceToken = deviceToken.split("#").join("");
    deviceToken = deviceToken.split("*").join("");
    deviceToken = deviceToken.split("&").join("");
    deviceToken = deviceToken.split("/").join("");
    if (deviceToken.length > 5) {
      deviceToken = deviceToken.slice(0, 255);
    }
    console.log(deviceToken);
    console.log(deviceToken.length);
    return deviceToken.toString();
  }

  connectToPushNotificationServer(DeviceToken) {
    sdk.notification.Connect();
    const mobileChannel = this.formatDeviceID(DeviceToken);
    // alert(mobileChannel);
    sdk.notification.SubscribeChannel(mobileChannel, callbackFunc)
  }


}
function callbackFunc(payload) {

  console.log("kien", payload.data.message);

  let message = payload.data.message;
  alert(message.content.msg);

  let data = JSON.parse(localStorage.getItem("Notification_History")) || [];
  data.push({
    datetime: message.datetime,
    orderID: message.content.orderID,
    message: message.content.msg,
    isNew: message.isNew
  });
  localStorage.setItem("Notification_History", JSON.stringify(data));

  let numberNoti: number = 0;

  let noti = JSON.parse(localStorage.getItem("Notification_History")) || [];
  for (let a of noti) {
    if (a.isNew == '1') {
      numberNoti += 1;
    }
  }

  let badge: Badge;
  badge.set(numberNoti);
}

